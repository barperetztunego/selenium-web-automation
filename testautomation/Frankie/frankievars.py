import random
import string
# Environment URLs
frankieUrl = 'https://test-uat.autotunego.com'

# Global
site_footer = '//*[@id="root"]/div[3]/div/button'
profile_header = '/html/body/div/div[1]/div/div/div/div[1]/header/div/div[4]/button[2]/span[1]'
notifications = '/html/body/div/div[1]/div/div/div/div[1]/header/div/div[4]/button[1]'

# Nav
nav_vault = '/html/body/div/div[1]/div/div/div/div[1]/header/div/div[3]/button[1]/span[1]/p'
nav_distro = '/html/body/div/div[1]/div/div/div/div[1]/header/div/div[3]/button[2]'
nav_songreview = '/html/body/div/div[1]/div/div/div/div[1]/header/div/div[3]/button[4]/span[1]/p'
nav_support = '/html/body/div/div[1]/div/div/div/div[1]/header/div/div[3]/button[5]'
nav_create = '#artistCard > button' #CSS SELECTOR NOT XPATH
nav_create_artist = '#SpeedDialexample-actions > button:nth-child(1) > span.MuiFab-label > p'
nav_create_submit = '/html/body/div[3]/div[3]/div/form/div[2]/button[2]/span[1]'
nav_create_artist_val = '#artistCard > div > div.MuiCardContent-root > input'
nav_create_locker = '/html/body/div[2]/div[3]/div/ul/li[2]/div/span/p'
nav_create_file = '/html/body/div[2]/div[3]/div/ul/li[3]/div/span/p'
nav_create_submitcheck = '#artistCard > div > div.MuiCardActions-root.MuiCardActions-spacing > button:nth-child(2) > svg'
nav_notifications = '#accountTools' #CSS Selector

# Artist Profile
artist_create_locker = '/html/body/div[1]/div/div/div/div/div[2]/div/div/table/thead/tr[2]/th[1]/span/p'


# Breadcrumbs
crumb_profile = '/html/body/div/div[1]/div/div/div/div[2]/nav/ol/li/a'
crumb_artist = '/html/body/div/div[1]/div/div/div/nav/ol/li[3]/a'

# Registration
regis_getstarted = '/html/body/div/div[1]/div/div/div[1]/article/button[1]/span[1]'
regis_email = '//*[@id="registrationEmail"]'
regis_username_val = "bperetz+release@tunego.com"
regis_password_val = "Charlie999!"
regis_password = '//*[@id="registrationPassword"]'
regis_password_confirm = '//*[@id="confirmPassword"]'
regis_acceptterms = '/html/body/div/div[1]/div/div/div[2]/div/form/span[1]/span[1]/input'
regis_submit = '/html/body/div/div[1]/div/div/div[2]/div/form/button/span[1]'
login_username = '//*[@id="signInEmail"]'
login_username_val = "bperetz+release@tunego.com"
login_password = '//*[@id="signInPassword"]'
login_password_val = "Password1!"
login_submit = '/html/body/div/div[1]/div/div/div[1]/div/form/button/span[1]'

# Vault
vault_addartist = '/html/body/div/div/div/div/div/div[2]/div/div[1]/div[1]/button'
vault_artistcard = '#artistCardMedia'
vault_artistname = '//*[@id="artistName"]'
vault_artistname_val = "Bar's Automated Artist " + ''.join(random.choices(string.ascii_uppercase + string.digits, k=5))
vault_artistdesc = '//*[@id="description"]'
vault_submitnew = '/html/body/div[2]/div[3]/div/form/div[2]/button[2]'

# Locker
locker_headers = '/html/body/div/div[1]/div/div/div/div[2]/div/table/thead/tr[1]/th[1]'
locker_addbutton = '/html/body/div/div[1]/div/div/div/div[2]/div/table/thead/tr[2]/th[1]/span/p'
locker_newname = '//*[@id="lockerName"]'
locker_newname_val = 'Locker Pog'
locker_cancel = '/html/body/div[2]/div[3]/div/form/div[2]/button[1]'
locker_create = '/html/body/div[4]/div[3]/div/form/div[2]/button[2]/span[1]'
locker_createdesc = '/html/body/div[2]/div[3]/div/p'
locker_files = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[1]/div/div/img'
locker_file_options = '/html/body/div/div[1]/div/div/div/div[2]/div/table/tbody/tr[1]/td[4]/button/svg'
locker_rename_file = '/html/body/div[6]/div[3]/ul/div[1]/div/span'
locker_delete_file = 'body > div:nth-child(10) > div:nth-child(3) > ul:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1)'
locker_download_file = 'body > div:nth-child(10) > div:nth-child(3) > ul:nth-child(1) > li:nth-child(3) > div:nth-child(1) > span:nth-child(1)'
locker_metadata = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[2]/div/div/img'
locker_metadata_general = '/html/body/div/div[1]/div/div/div/div[2]/div/div/div[1]/div/div'
locker_metadata_contributor = '/html/body/div/div[1]/div/div/div/div[2]/div/div/div[2]/div/div'
locker_metadata_additional = '/html/body/div/div[1]/div/div/div/div[2]/div/div/div[3]/div/div'
locker_collaborators = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[3]/div/div/img'
locker_fingerprint = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[4]/div/div/img'
locker_splits = '//*[@id="root"]/div/div/div/div/div[2]/div/div/div[5]/div/div/img'
locker_collab_header = '/html/body/div/div[1]/div/div/div/div[2]/header/div/div/div/button[1]/span'
locker_collab_stream = '/html/body/div/div[1]/div/div/div/div[2]/header/div/div/div/button[2]/span'
locker_collab_add = '/html/body/div/div[1]/div/div/div/div[2]/div[1]/div/div/div/table/tbody/div/div/button/span[1]'
locker_collab_addemail = '/html/body/div[2]/div[3]/div/form/div[1]/div/div'
locker_collab_cancel = '/html/body/div[2]/div[3]/div/form/div[2]/button[1]'
locker_collab_submit = '/html/body/div[2]/div[3]/div/form/div[2]/button[1]'
locker_card = '#root > div > div > div > div > div:nth-child(2) > div > div > table > tbody > tr > td:nth-child(1)'

# Song Locker - Files page
files_filename_header = '#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > thead > tr:nth-child(1) > th:nth-child(1)'
files_fileformat_header = '#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > thead > tr:nth-child(1) > th:nth-child(2)'
files_filesize_header = '#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > thead > tr:nth-child(1) > th:nth-child(3)'
files_clicktoadd = '/html/body/div[1]/div/div/div/div/div[2]/section/div/input'
files_addfileinput = '//*[@id="root"]/div/div/div/div/div[2]/section/div/div/div/div/table/thead/tr[2]/th/span/p'
files_filename = '#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > tbody > tr > th'
files_fileformat = '#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > tbody > tr > td:nth-child(2)'
files_filesize = '#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > tbody > tr > td:nth-child(3)'
files_fileoptions = 'td.MuiTableCell-root:nth-child(4) > button:nth-child(1) > svg:nth-child(1)'
files_options_rename = '#simple-popover > div.MuiPaper-root.MuiPopover-paper.MuiPaper-elevation8.MuiPaper-rounded > ul > div:nth-child(1) > div > span'
files_options_rename_header = 'body > div.jss105681 > div.jss105682 > h4'
files_options_rename_desc = 'body > div.jss107581 > div.jss107582 > p'
files_options_rename_desc_text = "'TuneGO's patented Vault technology helps you protect your music rights. Safely invite people to securely collaborate on your music and track contributions and activity. Collaborators will have access to ALL of your Song Lokers in this profile. Want to invite collaboratorsto individual songs? Invite them to your Song Locker instead."
files_options_rename_input = '#artistName'
files_options_rename_submit = 'body > div.jss115941 > div.jss115942 > div > button.MuiButtonBase-root.MuiButton-root.MuiButton-text.jss115939 > span.MuiButton-label'
files_options_rename_cancel = 'body > div.jss118221 > div.jss118222 > div > button.MuiButtonBase-root.MuiButton-root.MuiButton-text.jss118218 > span.MuiButton-label'

# Profile / Account Settings
profile_name_enter = '//*[@id="name"]'
profile_location_enter = '//*[@id="location"]'
profile_location_country = '/html/body/div/div[1]/div/div/div/div[2]/div/form/div[1]/div/div'
profile_location_country_option = '/html/body/div[2]/div[3]/ul/li[6]'

# Song Reviews
sr_start = '/html/body/div/div[1]/div/div/div/div[2]/div[3]/div/div/button/span[1]'
sr_back = '/html/body/div/div[1]/div/div/ul/li[1]'
sr_back_bottom = '/html/body/div/div[1]/div/div/div[4]/button[1]'
sr_next = '/html/body/div/div[1]/div/div/div[4]/button[2]'
sr_jointunego = '/html/body/div/div[1]/div/div/div[1]/button/span[1]'
sr_myreviews = '/html/body/div/div[1]/div/div/div/div[2]/div[1]/button[1]'
sr_newreview = '/html/body/div/div[1]/div/div/div/div[2]/div[1]/button[2]'
sr_mixreview = '/html/body/div/div[1]/div/div/div[3]/div/div/div[1]/div[2]/button'
sr_crowdreview = '/html/body/div/div[1]/div/div/div[3]/div/div/div[2]/div[2]/button'
sr_industryreview = '/html/body/div/div[1]/div/div/div[3]/div/div/div[3]/div[2]/button'
sr_mixreview_header = '/html/body/div/div[1]/div/div/div[3]/div/div/div[1]/div[1]/h2'
sr_crowdreview_header = '/html/body/div/div[1]/div/div/div[3]/div/div/div[2]/div[1]/h2'
sr_industryreview_header = '/html/body/div/div[1]/div/div/div[3]/div/div/div[3]/div[1]/h2'
sr_mixprice = '/html/body/div/div[1]/div/div/div[3]/div/div/div[1]/div[2]/h2'
sr_crowdprice = '/html/body/div/div[1]/div/div/div[3]/div/div/div[2]/div[2]/h2'
sr_industryprice = '/html/body/div/div[1]/div/div/div[3]/div/div/div[3]/div[2]/h2'
sr_mixcount = '/html/body/div/div[1]/div/div/div[3]/div/div/div[1]/div[2]/p[1]'
sr_crowdcount = '/html/body/div/div[1]/div/div/div[3]/div/div/div[1]/div[2]/p[1]'
sr_industrycount = '/html/body/div/div[1]/div/div/div[3]/div/div/div[3]/div[2]/p[1]'
sr_mixp1 = '/html/body/div/div[1]/div/div/div[3]/div/div/div[1]/div[2]/p[2]'
sr_mixp2 = '/html/body/div/div[1]/div/div/div[3]/div/div/div[1]/div[2]/p[3]'
sr_crowdp1 = '/html/body/div/div[1]/div/div/div[3]/div/div/div[2]/div[2]/p[2]'
sr_crowdp2 = '/html/body/div/div[1]/div/div/div[3]/div/div/div[2]/div[2]/p[3]'
sr_industryp1 = '/html/body/div/div[1]/div/div/div[3]/div/div/div[3]/div[2]/p[2]'
sr_industryp2 = '/html/body/div/div[1]/div/div/div[3]/div/div/div[3]/div[2]/p[3]'
sr_selectsong_header = '/html/body/div/div[1]/div/div/div[3]/div/h2'
sr_next2 = '/html/body/div/div[1]/div/div/div[4]/button[2]'


# Errors
login_err1 = "Password must be at least 8 characters."
login_err2 = "Please enter a valid email address."
