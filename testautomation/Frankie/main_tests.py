import time
import unittest
import frankievars
import random
import string
from seleniumwire import webdriver
import requests
import locker_page_tests
import vault_page_tests
import front_page_tests
import review_page_tests
import settings_page_tests


class MainTests(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(
            seleniumwire_options={'verify_ssl': False}
        )
        self.browser.maximize_window()

    def testPageTitle(self):
        self.browser.get(frankievars.frankieUrl)
        self.assertIn(':: TuneGO ::', self.browser.title)


    def testVault(self):
        vault_page_tests.VaultPageTests.login()
        #self.verifyVaultUI()
        vault_page_tests.VaultPageTests.createArtist()
        vault_page_tests.VaultPageTests.enterArtist()
        time.sleep(15)


    def testLockers(self):
        locker_page_tests.LockerPageTests.login(self)
        vault_page_tests.VaultPageTests.enterArtist(self)
        locker_page_tests.LockerPageTests.createLocker(self)
        locker_page_tests.LockerPageTests.enterLocker(self)
        locker_page_tests.LockerPageTests.lockerUploadFiles(self)
        locker_page_tests.LockerPageTests.renameLockerFile(self)

    def getVisibleElementWithName(self, name):
        fields = self.browser.find_elements_by_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def getVisibleElementWithClassName(self, name):
        fields = self.browser.find_elements_by_class_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None


if __name__ == '__main__':
    unittest.main(verbosity=2)