import time
import unittest
import frankievars
import random
import string
from seleniumwire import webdriver
import requests


class VaultPageTests(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(
            seleniumwire_options={'verify_ssl': False}
        )
        self.browser.maximize_window()

    def testPageTitle(self):
        self.browser.get(frankievars.frankieUrl)
        self.assertIn(':: TuneGO ::', self.browser.title)

    def testVault(self):
        self.login()
        #self.verifyVaultUI()
        self.createArtist()
        self.enterArtist()
        time.sleep(15)

    def getVisibleElementWithName(self, name):
        fields = self.browser.find_elements_by_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def getVisibleElementWithClassName(self, name):
        fields = self.browser.find_elements_by_class_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    #  WORKS
    def login(self):
        self.browser.get(frankievars.frankieUrl)
        username = self.browser.find_element_by_xpath(frankievars.login_username)
        username.send_keys(frankievars.login_username_val)
        time.sleep(3)
        password = self.browser.find_element_by_xpath(frankievars.login_password)
        password.send_keys(frankievars.login_password_val)
        time.sleep(3)
        submit = self.browser.find_element_by_xpath(frankievars.login_submit)
        submit.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def createFile(self):
        # Uses the +Create button, not the "Add Artist Profile" card
        time.sleep(10)
        self.enterLocker()
        clickCreate = self.browser.find_element_by_xpath(frankievars.artist_create_locker)
        clickCreate.click()
        clickFile = self.browser.find_element_by_xpath(frankievars.nav_create_file)
        clickFile.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    # WORKS
    def createArtist(self):
        # Uses the +Create button, not the "Add Artist Profile" card
        time.sleep(10)
        # Breaks here
        clickCreate = self.browser.find_element_by_css_selector(frankievars.nav_create)
        clickCreate.click()
        time.sleep(3)
        artistName = self.browser.find_element_by_css_selector(frankievars.nav_create_artist_val)
        artistName.send_keys(frankievars.vault_artistname_val)
        submitNew = self.browser.find_element_by_css_selector(frankievars.nav_create_submitcheck)
        submitNew.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    # WORKS
    def createLocker(self):
        # Uses the +Create button, not the "Add Song Locker" card
        time.sleep(10)
        clickCreate = self.browser.find_element_by_xpath(frankievars.artist_create_locker)
        clickCreate.click()
        time.sleep(3)
        lockerName = self.browser.find_element_by_xpath(frankievars.locker_newname)
        lockerName.send_keys(frankievars.locker_newname_val)
        submit = self.browser.find_element_by_xpath(frankievars.locker_create)
        submit.click()
        time.sleep(15)
        self.assertIn(frankievars.locker_newname, self.browser.page_source)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    # WORKS
    def addArtist(self):
        time.sleep(5)
        addArtist = self.browser.find_element_by_xpath(frankievars.vault_addartist)
        addArtist.click()
        artistName = self.browser.find_element_by_xpath(frankievars.nav_create_artist_val)
        artistName.send_keys(frankievars.vault_artistname_val)
        submitNew = self.browser.find_element_by_xpath(frankievars.nav_create_submit)
        submitNew.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    # WORKS
    def enterArtist(self):
        time.sleep(5)
        enterArtist = self.browser.find_element_by_css_selector(frankievars.vault_artistcard)
        enterArtist.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    # IN PROGRESS
    def enterLocker(self):
        time.sleep(10)
        enterLocker = self.browser.find_element_by_css_selector(frankievars.locker_card)
        enterLocker.click()
        time.sleep(5)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def verifyVaultUI(self):
        pageSource = self.browser.page_source
        currentUrl = self.browser.current_url
        self.assertIn('/artists', currentUrl)
        self.assertIn(frankievars.nav_vault, pageSource)
        self.assertIn(frankievars.nav_distro, pageSource)
        self.assertIn(frankievars.nav_songreview, pageSource)
        self.assertIn(frankievars.nav_support, pageSource)
        self.assertIn(frankievars.nav_notifications, pageSource)

if __name__ == '__main__':
    unittest.main(verbosity=2)