import random
import string
import time
import unittest
import frankievars
from seleniumwire import webdriver


class LockerPageTests(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(
            seleniumwire_options={'verify_ssl': False}
        )
        self.browser.maximize_window()

    def testPageTitle(self):
        self.browser.get(frankievars.frankieUrl)
        self.assertIn(':: TuneGO ::', self.browser.title)

    def getVisibleElementWithName(self, name):
        fields = self.browser.find_elements_by_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def getVisibleElementWithClassName(self, name):
        fields = self.browser.find_elements_by_class_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def login(self):
        self.browser.get(frankievars.frankieUrl)
        username = self.browser.find_element_by_xpath(frankievars.login_username)
        username.send_keys(frankievars.login_username_val)
        time.sleep(3)
        password = self.browser.find_element_by_xpath(frankievars.login_password)
        password.send_keys(frankievars.login_password_val)
        time.sleep(3)
        submit = self.browser.find_element_by_xpath(frankievars.login_submit)
        submit.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Logged in - SUCCESS.")



    # WORKS
    def createArtist(self):
        # Uses the +Create button, not the "Add Artist Profile" card
        time.sleep(5)
        # Breaks here
        clickCreate = self.browser.find_element_by_css_selector(frankievars.nav_create)
        clickCreate.click()
        time.sleep(3)
        artistName = self.browser.find_element_by_css_selector(frankievars.nav_create_artist_val)
        artistName.send_keys(frankievars.vault_artistname_val)
        submitNew = self.browser.find_element_by_css_selector(frankievars.nav_create_submitcheck)
        submitNew.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Created Artist Profile " + frankievars.nav_create_artist_val, " - SUCCESS")



    # WORKS
    def createLocker(self):
        # Uses the +Create button, not the "Add Song Locker" card
        time.sleep(5)
        clickCreate = self.browser.find_element_by_xpath(frankievars.artist_create_locker)
        clickCreate.click()
        time.sleep(3)
        lockerName = self.browser.find_element_by_xpath(frankievars.locker_newname)
        lockerName.send_keys(frankievars.locker_newname_val)
        submit = self.browser.find_element_by_xpath(frankievars.locker_create)
        submit.click()
        time.sleep(15)
        print("Created Locker " + frankievars.locker_newname_val, ".")
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("TEST: Create Locker Test - Success.")

        # WORKS
    def enterArtist(self):
        time.sleep(15)
        enterArtist = self.browser.find_element_by_css_selector(frankievars.vault_artistcard)
        enterArtist.click()
        time.sleep(10)
        print("Entered Artist Profile.")
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("TEST: Entered Artist Profile TEST - SUCCESS.")

    # IN PROGRESS
    def enterLocker(self):
        time.sleep(5)
        enterLocker = self.browser.find_element_by_css_selector(frankievars.locker_card)
        enterLocker.click()
        time.sleep(5)
        print("Entered Locker.")
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )
        print("TEST: Enter Locker Test- SUCCESS.")

    def enterLockerTabs(self):
        time.sleep(5)
        fileTile = self.browser.find_element_by_xpath(frankievars.locker_files)
        fileTile.click()
        print("TEST: Enter Files tab - SUCCESS")
        time.sleep(5)
        self.browser.back()
        time.sleep(2)
        metaTile = self.browser.find_element_by_xpath(frankievars.locker_metadata)
        metaTile.click()
        print("TEST: Enter Metadata tab - SUCCESS")
        time.sleep(5)
        self.browser.back()
        time.sleep(2)
        collabTile = self.browser.find_element_by_xpath(frankievars.locker_collaborators)
        collabTile.click()
        print("TEST: Enter Collaborators tab - SUCCESS")
        time.sleep(5)
        self.browser.back()
        time.sleep(2)
        fingerprintTile = self.browser.find_element_by_xpath(frankievars.locker_fingerprint)
        fingerprintTile.click()
        print("TEST: Enter Fingerprint tab - SUCCESS")
        time.sleep(5)
        self.browser.back()
        time.sleep(2)
        splitsTile = self.browser.find_element_by_xpath(frankievars.locker_splits)
        splitsTile.click()
        time.sleep(5)

        for i in range(3):
            self.browser.back()

        time.sleep(30)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("TEST: Checked Locker Tabs - SUCCESS.")

    def lockerUploadFiles(self):
        time.sleep(5)
        fileTile = self.browser.find_element_by_xpath(frankievars.locker_files)
        fileTile.click()
        time.sleep(5)
        fileNameheader = self.browser.find_element_by_css_selector(frankievars.files_filename_header)
        fileFormatheader = self.browser.find_element_by_css_selector(frankievars.files_fileformat_header)
        fileSizeheader = self.browser.find_element_by_css_selector(frankievars.files_filesize_header)
        addFilebutton = self.browser.find_element_by_xpath(frankievars.files_clicktoadd)
        self.assertEqual(fileNameheader.text, "File Name")
        self.assertEqual(fileFormatheader.text, "File Format")
        self.assertEqual(fileSizeheader.text, "File Size")
        print("TEST Files Headers Test - SUCCESS")
        time.sleep(5)
        #addFile = self.browser.find_element_by_xpath(frankievars.files_addfileinput)
        filekey = '/Users/barperetz/Downloads/Ensoniq-ESQ-1-Sympy-C4(2).wav'
        addFilebutton.send_keys(filekey)
        time.sleep(10)
        uploadedFile = self.browser.find_element_by_css_selector('#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > tbody > tr > th')
        self.assertEqual(uploadedFile.text, 'Ensoniq-ESQ-1-Sympy-C4(2).wav')
        fileFormat = self.browser.find_element_by_css_selector(frankievars.files_fileformat)
        self.assertEqual(fileFormat.text, 'audio/wave')
        fileSize = self.browser.find_element_by_css_selector(frankievars.files_filesize)
        self.assertNotEqual(fileSize.text, "")
        time.sleep(10)

        print("TEST: Upload Files Test - SUCCESS")

    def renameLockerFile(self):
        fileOptions = self.browser.find_element_by_css_selector(frankievars.files_fileoptions)
        fileOptions.click()
        time.sleep(5)
        renameButton = self.browser.find_element_by_css_selector(frankievars.files_options_rename)
        renameButton.click()
        time.sleep(2)
        self.assertEqual(frankievars.files_options_rename_header.text, "Rename your File")
        self.assertEqual(frankievars.files_options_rename_desc.text, frankievars.files_options_rename_desc_text)
        renameInput = self.browser.find_element_by_css_selector(frankievars.files_options_rename_input)
        renameInput.send_keys("AUTORENAMED")
        cancelButton = self.browser.find_element_by_css_selector(frankievars.files_options_rename_cancel)
        cancelButton.click()
        time.sleep(5)
        fileOptions = self.browser.find_element_by_css_selector(frankievars.files_fileoptions)
        fileOptions.click()
        time.sleep(5)
        renameButton = self.browser.find_element_by_css_selector(frankievars.files_options_rename)
        renameButton.click()
        time.sleep(2)
        renameInput = self.browser.find_element_by_css_selector(frankievars.files_options_rename_input)
        renameInput.send_keys("AUTORENAMED")
        submitButton = self.browser.find_element_by_css_selector(frankievars.files_options_rename_submit)
        submitButton.click()
        time.sleep(10)
        uploadedFile = self.browser.find_element_by_css_selector('#root > div > div > div > div > div:nth-child(2) > section > div > div > div > div > table > tbody > tr > th')
        self.assertEqual(uploadedFile.text, "AUTORENAMED")

    def verifyVaultUI(self):
        pageSource = self.browser.page_source
        currentUrl = self.browser.current_url
        self.assertIn('/artists', currentUrl)
        self.assertIn(frankievars.nav_vault, pageSource)
        self.assertIn(frankievars.nav_distro, pageSource)
        self.assertIn(frankievars.nav_songreview, pageSource)
        self.assertIn(frankievars.nav_support, pageSource)
        self.assertIn(frankievars.nav_notifications, pageSource)

if __name__ == '__main__':
    unittest.main(verbosity=2)