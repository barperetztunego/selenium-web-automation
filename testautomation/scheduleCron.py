import datetime
from crontab import CronTab

my_cron = CronTab(user='barperetz')

for job in my_cron:
    print(job)

job = my_cron.new(command='python artists_page_tests.py')
job.minute.every(1)
sch = job.schedule(date_from=datetime.datetime.now())

my_cron.write()
