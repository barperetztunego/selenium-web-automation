import unittest
import artists_page_tests as artisttests
from selenium import webdriver


class MainTests(unittest.TestCase):

    def getVisibleElementWithName(self, name):
        fields = self.browser.find_elements_by_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def getVisibleElementWithClassName(self, name):
        fields = self.browser.find_elements_by_class_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def setUp(self):
        self.browser = webdriver.Chrome()

    def testMain(self):
        #artisttests.ArtistsPageTests.testCreateNewArtist(self)
        artisttests.ArtistsPageTests.testArtistOptions(self)

    def tearDown(self):
        self.browser.quit()

if __name__ == '__main__':
    unittest.main(verbosity=2)