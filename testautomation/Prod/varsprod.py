# Environment URLs
prodUrl = "https://www.tunego.com"

# Global
site_footer = '//*[@id="root"]/div[3]/div/button'

# Registration
getStarted = '/html/body/div[1]/div[1]/section[1]/article[1]/button[2]'
regis_email = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[1]'
regis_pass = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[2]'
regis_passconfirm = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[3]'
regis_terms = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[4]'
regis_submit = '/html/body/div[1]/div[1]/section[2]/article[2]/form/button'

# Login
login_user = '/html/body/div[1]/div[1]/section[1]/article[2]/form/input[1]'
login_pass = '/html/body/div[1]/div[1]/section[1]/article[2]/form/input[2]'
login_submit = '/html/body/div[1]/div[1]/section[1]/article[2]/form/button'

# Errors
login_err1 = 'Password must be at least 8 characters.'
login_err2 = 'Please enter a valid email address.'