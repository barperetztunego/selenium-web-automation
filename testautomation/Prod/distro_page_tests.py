import unittest
import time
import varsue
from seleniumwire import webdriver
from selenium.webdriver.common.action_chains import ActionChains


class DistroPageTests(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(
            seleniumwire_options={'verify_ssl': False}
        )
        self.browser.maximize_window()

    def testStartDistro(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(10)
        distroNav = self.browser.find_element_by_xpath(varsue.distro_nav)
        distroNav.click()
        time.sleep(5)
        distroTitle = self.browser.find_element_by_xpath(varsue.distro_title)
        distroSubtitle = self.browser.find_element_by_xpath(varsue.distro_subtitle)
        self.assertEqual(varsue.distro_title_text, distroTitle.text)
        # EDIT "plusover" to "plus over" once bug is Fixed on Web
        #self.assertEqual(
        #    "Get your music on the world's leading music stores, including Spotify, Apple Music, Amazon Music, TIDAL, "
        #    "Deezer, Shazam, plusover 90 more outlets.",
        #    distroSubtitle.text
        #)
        enterDistro = self.browser.find_element_by_xpath(varsue.distro_start)
        enterDistro.click()
        time.sleep(5)
        self.assertIn(varsue.distro_artisturl, self.browser.current_url)
        distroHeader = self.browser.find_element_by_xpath(varsue.distro_header)
        self.assertEqual(varsue.distro_header_text, distroHeader.text)
        distroDescription = self.browser.find_element_by_xpath(varsue.distro_description)
        self.assertEqual(
            varsue.distro_description_text,
            distroDescription.text
        )
        addProfileLabel = self.browser.find_element_by_xpath(varsue.distro_newartist)
        self.assertEqual(varsue.distro_newartist_text, addProfileLabel.text)
        displayNameIn = self.browser.find_element_by_xpath(varsue.distro_newname)
        displayNameIn.click()
        displayNameIn.send_keys(varsue.distro_newname_text)
        #Bug creating Artists in Distro100 - UEDev
        #distroCreateArtist = self.browser.find_element_by_xpath(varsue.distro_createartist)
        #distroCreateArtist.click()
        time.sleep(3)
        selectArtist = self.browser.find_elements_by_class_name(varsue.distro_selectartist)[1]
        selectArtist.click()
        time.sleep(5)
        for i in range(2):
            removeTrack = self.browser.find_element_by_xpath(varsue.distro_removetrack)
            removeTrack.click()
        time.sleep(5)
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        nextPage = self.browser.find_element_by_xpath(varsue.distro_nextpage)
        nextPage.click()
        time.sleep(5)
        releaseName = self.browser.find_element_by_xpath(varsue.distro_releasename)
        releaseName.send_keys(varsue.distro_releasename_text)
        releaseVer = self.browser.find_element_by_xpath(varsue.distro_releaseversion)
        releaseVer.send_keys(varsue.distro_releaseversion_text)
        featuredArtist = self.browser.find_element_by_xpath(varsue.distro_featuredartist)
        featuredArtist.send_keys(varsue.distro_featuredartist_text)
        label = self.browser.find_element_by_xpath(varsue.distro_label)
        label.click()
        labelOption = self.browser.find_element_by_xpath(varsue.distro_label_option)
        labelOption.click()
        self.browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(5)
        albumType = self.browser.find_element_by_xpath(varsue.distro_albumtype)
        albumType.click()
        albumtypeOption = self.browser.find_element_by_xpath(varsue.distro_albumtype_option)
        albumtypeOption.click()
        footer = self.browser.find_element_by_xpath(varsue.site_footer)
        footer.click()
        explicit = self.browser.find_element_by_xpath(varsue.distro_explicit)
        explicit.click()
        explicitOption = self.browser.find_element_by_xpath(varsue.distro_explicit_option)
        explicitOption.click()
        upc = self.browser.find_element_by_xpath(varsue.distro_upc)
        upc.send_keys(varsue.distro_upc_value)
        recYear = self.browser.find_element_by_xpath(varsue.distro_recyear)
        recYear.click()
        recyearOption = self.browser.find_element_by_xpath(varsue.distro_recyear_option)
        recyearOption.click()
        genre = self.browser.find_element_by_xpath(varsue.distro_genre)
        genre.click()
        genreOption = self.browser.find_element_by_xpath(varsue.distro_genre_option)
        genreOption.click()
        secGenre = self.browser.find_element_by_xpath(varsue.distro_secgenre)
        secGenre.click()
        secgenreOption = self.browser.find_element_by_xpath(varsue.distro_secgenre_option)
        secgenreOption.click()
        time.sleep(3)
        copyrightInfo = self.browser.find_element_by_xpath(varsue.distro_copyright)
        copyrightInfo.send_keys(varsue.distro_copyright_value)
        labelInfo = self.browser.find_element_by_xpath(varsue.distro_labelcopy)
        labelInfo.send_keys(varsue.distro_labelcopy_text)
        save = self.browser.find_element_by_xpath(varsue.distro_save)
        save.click()
        time.sleep(5)
        for i in range(4):
            confirm = self.browser.find_elements_by_class_name('checkmark')
            confirm[i].click()
        expliconfirm = self.browser.find_element_by_xpath(varsue.distro_explicitconfirm)
        expliconfirm.click()
        expliconfirmOption = self.browser.find_element_by_xpath(varsue.distro_explicitconfirm_option)
        expliconfirmOption.click()
        next2 = self.browser.find_element_by_xpath(varsue.distro_nextpage2)
        next2.click()
        time.sleep(10)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testLearnMore(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(10)
        footer = self.browser.find_element_by_xpath(varsue.site_footer)
        footer.click()
        distroNav = self.browser.find_element_by_xpath(varsue.distro_nav)
        distroNav.click()
        time.sleep(5)
        distroTitle = self.browser.find_element_by_xpath(varsue.distro_title)
        distroSubtitle = self.browser.find_element_by_xpath(varsue.distro_subtitle)
        self.assertEqual(varsue.distro_title_text, distroTitle.text)
        enterDistro = self.browser.find_element_by_xpath(varsue.distro_start)
        enterDistro.click()
        time.sleep(5)
        learnMore = self.browser.find_element_by_xpath(varsue.distro_learnmore)
        actions = ActionChains(self.browser)
        actions.move_to_element(learnMore).perform()
        learnMore.click()
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )
        print("Test completed.")

    def tearDown(self):
        self.browser.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)