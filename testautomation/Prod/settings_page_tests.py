import unittest
import time
import varsue
from seleniumwire import webdriver
from selenium import webdriver


class SettingsPageTests(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(
            seleniumwire_options={'verify_ssl': False}
        )

    def testAPageTitle(self):
        self.browser.get(varsue.devUrl)
        self.assertIn(':: TuneGO ::', self.browser.title)

    def getVisibleElementWithName(self, name):
        fields = self.browser.find_elements_by_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def getVisibleElementWithClassName(self, name):
        fields = self.browser.find_elements_by_class_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def testBLogin(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testChangeName(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        enterProfile = self.browser.find_element_by_xpath(varsue.profile_enter)
        enterProfile.click()
        time.sleep(5)
        enterChangeName = self.browser.find_element_by_xpath(varsue.profile_changename)
        enterChangeName.click()
        time.sleep(5)
        firstName = self.browser.find_element_by_xpath(varsue.profile_firstname)
        firstName.click()
        firstName.clear()
        firstName.send_keys(varsue.profile_firstname_text)
        lastName = self.browser.find_element_by_xpath(varsue.profile_lastname)
        lastName.click()
        lastName.clear()
        lastName.send_keys(varsue.profile_lastname_text)
        time.sleep(5)
        submitChanges = self.browser.find_element_by_xpath(varsue.profile_savechanges)
        submitChanges.click()
        time.sleep(5)
        fullName = self.browser.find_element_by_xpath(varsue.profile_fullname)
        fullName.click()
        time.sleep(3)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testChangeLocation(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        enterProfile = self.browser.find_element_by_xpath(varsue.vault_clickartist)
        enterProfile.click()
        time.sleep(5)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")


if __name__ == '__main__':
    unittest.main(verbosity=2)