# Environment URLs
devUrl = 'https://www.uedev.autotunego.com'
uatUrl = 'https://www.uat.autotunego.com'

# Global
site_footer = '//*[@id="root"]/div[3]/div/button'

# Registration
regis_getstarted = '/html/body/div[1]/div[1]/section[1]/article[1]/button[2]'
regis_username = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[1]'
regis_username_text = "AutoName"
regis_password_text = "Autopass123!"
regis_password = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[2]'
regis_password_confirm = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[3]'
regis_acceptterms = '/html/body/div[1]/div[1]/section[2]/article[2]/form/input[4]'
regis_submit = '/html/body/div[1]/div[1]/section[2]/article[2]/form/button'


# Login
username_value = 'BPeretz+willcall15@tunego.com'
password_value = 'Jasmine123!'
login_username = '/html/body/div[1]/div[1]/section[1]/article[2]/form/input[1]'
login_password = '/html/body/div[1]/div[1]/section[1]/article[2]/form/input[2]'
login_submit = '//*[@id="root"]/div[1]/section[1]/article[2]/form/button'

# Distro100
distro_nav = '/html/body/div[1]/div[4]/div/div[2]/div/div[2]/img'
distro_title = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div/div/div[2]/div[1]'
distro_title_text = "Distribute music now"
distro_header_text = "Distribute New Music"
distro_subtitle = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div/div/div[2]/div[2]'
distro_subtitle__text = "Get your music on the world's leading music stores, including Spotify, Apple Music, Amazon Music, TIDAL, Deezer, Shazam, plusover 90 more outlets."
distro_start = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div/div/div[2]/button'
distro_artisturl = "https://www.uedev.autotunego.com/distribution/select-artist"
distro_header = '/html/body/div[1]/div[4]/div/div[2]/main/div[2]/div/h2'
distro_description = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div/div[2]/div[1]'
distro_description_text = "To Begin your distribution process, simply select an artist profile that you have previously created or create a new one here. This is where we will store and gather all of your distribution information for you."
distro_newartist = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div/div[2]/div[3]/div/p'
distro_newartist_text = "Enter A New Artist Name or Choose An Existing One"
distro_createartist = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div/div[2]/div[3]/div/div/div[2]/button'
distro_newname = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div/div[2]/div[3]/div/div/div[1]/input'
distro_newname_text = "Automated Name"
distro_selectartist = 'artist-selected' # Class name [1] to access 1st artist in list
distro_nextpage = '/html/body/div[1]/div[4]/div/div[2]/main/div[2]/div[4]/button[2]'
distro_nextpage2 = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[3]/button[3]'
distro_learnmore = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div/div[3]/button'
distro_removetrack = '/html/body/div[1]/div[4]/div/div[2]/main/div[2]/div[2]/ul/li[2]/div/div/div[5]/a'
distro_trackstatus = '/html/body/div[1]/div[4]/div/div[2]/main/div[2]/div[2]/ul/li[3]/div/div/div[4]'
distro_releasename = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/input[1]'
distro_releasename_text = "Auto Release by Bar"
distro_releaseversion = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/input[2]'
distro_releaseversion_text = "2"
distro_featuredartist = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/input[4]'
distro_featuredartist_text = "Bar Peretz"
distro_label = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[1]'
distro_label_option = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[1]/option[2]'
distro_albumtype = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[2]'
distro_albumtype_option = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[2]/option[2]'
distro_explicit = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[3]'
distro_explicit_option = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[3]/option[2]'
distro_upc = '//*[@id="root"]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/input[5]'
distro_upc_value = "1234567890"
distro_recyear = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[4]'
distro_recyear_option = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[4]/option[12]'
distro_genre = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[5]'
distro_genre_option = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[5]/option[2]'
distro_secgenre = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[6]'
distro_secgenre_option = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/select[6]/option[2]'
distro_labelinfo = '//*[@id="root"]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/input[6]'
distro_labelinfo_value = "LABEL-999"
distro_labelcopy = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/input[6]'
distro_labelcopy_text = "TEST"
distro_copyright = '//*[@id="root"]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[1]/input[7]' # CSS SELECTOR NOT XPATH
distro_copyright_value = "1999 Pokemon Inc"
distro_save = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[3]/button[1]'
distro_confirmchecks = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div[2]/label[1]/span'
distro_explicitconfirm = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div[2]/label[5]/select'
distro_explicitconfirm_option = '/html/body/div[1]/div[4]/div/div[2]/div[2]/div[2]/div/div[2]/div[2]/div[2]/label[5]/select/option[2]'

# Vault / Artist Profiles
vault_clickartist = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[2]/section'
vault_newartist = '/html/body/div[1]/div[4]/div/div[2]/main/div[2]/div[1]/button'
vault_newartist_input = '/html/body/div[1]/div[4]/div/div[2]/main/div[7]/div/div/input'
vault_autoname = "AUTOMATED NAME"
vault_newartist_submit = '/html/body/div[1]/div[4]/div/div[2]/main/div[7]/div/div/div[2]/button[2]'

# Song Locker
locker_clicklocker = '//*[@id="root"]/div[5]/div/div[2]/main/div[7]/div[2]/div/section/div[1]'
vault_enterinspirations = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div/section/div/img[2]'
locker_addinspiration = '/html/body/div[1]/div[4]/div/div[2]/main/div[4]/div/button'
locker_newlocker = '/html/body/div[1]/div[4]/div/div[2]/main/div[7]/div[1]/div/section/div'
locker_newname = '/html/body/div[1]/div[4]/div/div[2]/main/div[6]/div/div/input'
locker_newname_text = "Bar's Automagic Locker"
locker_submitnew = '/html/body/div[1]/div[4]/div/div[2]/main/div[6]/div/div/div[2]/button[2]'
locker_fingerprint = 'div.locker-label:nth-child(2)' # CSS SELECTOR NOT XPATH
locker_files = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/img'
locker_metadata = 'div.locker-tile:nth-child(2) > img:nth-child(1)' # CSS SELECTOR NOT XPATH

# Song Locker - Metadata
meta_generaltab = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[1]/div/span'
meta_artistname = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[2]/div/input'
meta_artistname_text = "Bar's Automated Name"
meta_songname = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[3]/div/div'
meta_songname_text = "Bar's Auto Song Name"
meta_version = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[4]/div/select'
meta_version_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[4]/div/select/option[2]'
meta_primarygenre = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[5]/div[1]/select'
meta_primarygenre_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[5]/div[1]/select/option[2]'
meta_secondarygenre = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[5]/div[2]/select'
meta_secondarygenre_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[5]/div[2]/select/option[2]'
meta_beenDistrib = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[8]/div/select'
meta_beenDistrib_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[8]/div/select/option[2]'
meta_datalang = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[9]/div/select'
meta_datalong_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[9]/div/select/option[2]'
meta_recordlang = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[10]/div/select'
meta_recordlang_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[10]/div/select/option[2]'
meta_explicit = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[11]/div/select'
meta_explicit_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[11]/div/select/option[2]'
meta_labelq = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[12]/div/select'
meta_labelq_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[12]/div/select/option[2]'
meta_publisherq = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[13]/div/select'
meta_publisherq_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[13]/div/select/option[2]'
meta_tracktype = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[14]/div/select'
meta_tracktype_option = '/html/body/div[1]/div[4]/div/div[2]/main/div[3]/div[1]/div[1]/div[2]/div[14]/div/select/option[2]'

# Settings / Profile
profile_enter = '/html/body/div[1]/div[5]/div/div[1]/div/div[3]/div[3]'
profile_changename = '/html/body/div[1]/div[5]/div/div[2]/main/div[2]/div/input[1]'
profile_firstname = '/html/body/div[1]/div[5]/div/div[2]/main/div[2]/div/input[1]'
profile_firstname_text = "Auto First Name"
profile_lastname = '/html/body/div[1]/div[5]/div/div[2]/main/div[2]/div/input[2]'
profile_lastname_text = "Auto Last Name:"
profile_savechanges = '/html/body/div[1]/div[5]/div/div[2]/main/div[2]/div/div[3]/button[2]'
profile_fullname = '/html/body/div[1]/div[5]/div/div[2]/main/div[2]/div/input[1]'


# Errors
login_err1 = "Password must be at least 8 characters."
login_err2 = "Please enter a valid email address."