import unittest
import time
import varsue
from seleniumwire import webdriver


class LockerPageTests(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(
            seleniumwire_options={'verify_ssl': False}
        )

    def testAPageTitle(self, devUrl):
        self.browser.get(devUrl)
        self.assertIn(':: TuneGO ::', self.browser.title)

    def getVisibleElementWithName(self, name):
        fields = self.browser.find_elements_by_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def getVisibleElementWithClassName(self, name):
        fields = self.browser.find_elements_by_class_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def testBLogin(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")


    def testInspirations(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(10)
        enterInspirations = self.browser.find_element_by_xpath(varsue.vault_enterinspirations)
        enterInspirations.click()
        time.sleep(3)
        addInspiration = self.browser.find_element_by_xpath(varsue.locker_addinspiration)
        addInspiration.click()
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testCreateNewLocker(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(10)
        artist = self.browser.find_element_by_xpath(varsue.vault_clickartist)
        artist.click()
        time.sleep(10)
        newLocker = self.browser.find_element_by_xpath(varsue.locker_newlocker)
        newLocker.click()
        time.sleep(10)
        newLockerName = self.browser.find_element_by_xpath(varsue.locker_newname)
        newLockerName.send_keys(varsue.locker_newname_text)
        time.sleep(10)
        submitNewLocker = self.browser.find_element_by_xpath(varsue.locker_submitnew)
        submitNewLocker.click()
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testEnterLockerFingerprint(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        profile = self.browser.find_element_by_xpath(varsue.vault_clickartist)
        profile.click()
        time.sleep(3)
        locker = self.browser.find_element_by_xpath(varsue.locker_clicklocker)
        locker.click()
        time.sleep(3)
        fingerprint = self.browser.find_element_by_css_selector(varsue.locker_fingerprint)
        fingerprint.click()
        time.sleep(5)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testEnterLockerFiles(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        profile = self.browser.find_element_by_xpath(varsue.vault_clickartist)
        profile.click()
        time.sleep(3)
        locker = self.browser.find_element_by_xpath(varsue.locker_clicklocker)
        locker.click()
        time.sleep(10)
        fileTile = self.browser.find_element_by_xpath(varsue.locker_files)
        fileTile.click()
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testEnterLockerMetadata(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        profile = self.browser.find_element_by_xpath(varsue.vault_clickartist)
        profile.click()
        time.sleep(10)
        locker = self.browser.find_element_by_xpath(varsue.locker_clicklocker)
        locker.click()
        time.sleep(10)
        metaTile = self.browser.find_element_by_css_selector(varsue.locker_metadata)
        metaTile.click()
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testEditLockerGenData(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        profile = self.browser.find_element_by_xpath(varsue.vault_clickartist)
        profile.click()
        time.sleep(10)
        try:
            locker = self.browser.find_element_by_xpath(varsue.locker_clicklocker)
            locker.click()
        except:
            newLocker = self.browser.find_element_by_xpath(varsue.locker_newlocker)
            newLocker.click()
            time.sleep(5)
            newLockerName = self.browser.find_element_by_xpath(varsue.locker_newname)
            newLockerName.send_keys(varsue.locker_newname_text)
            time.sleep(5)
            submitNewLocker = self.browser.find_element_by_xpath(varsue.locker_submitnew)
            submitNewLocker.click()
            time.sleep(5)
            locker = self.browser.find_element_by_xpath(varsue.locker_clicklocker)
            locker.click()

        time.sleep(10)
        metaTile = self.browser.find_element_by_css_selector(varsue.locker_metadata)
        metaTile.click()
        time.sleep(10)
        generalTab = self.browser.find_element_by_xpath(varsue.meta_generaltab)
        generalTab.click()
        time.sleep(5)
        artistName = self.browser.find_element_by_xpath(varsue.meta_artistname)
        artistName.clear()
        artistName.send_keys(varsue.meta_artistname_text)
        time.sleep(5)
        songName = self.browser.find_element_by_xpath(varsue.meta_songname)
        songName.send_keys(varsue.meta_songname_text)
        version = self.browser.find_element_by_xpath(varsue.meta_version)
        version.click()
        verOption = self.browser.find_element_by_xpath(varsue.meta_version_option)
        verOption.click()
        primGenre = self.browser.find_element_by_xpath(varsue.meta_primarygenre)
        primGenre.click()
        primGenreOption = self.browser.find_element_by_xpath(varsue.meta_primarygenre_option)
        primGenreOption.click()
        secGenre = self.browser.find_element_by_xpath(varsue.meta_secondarygenre)
        secGenre.click()
        secGenreOption = self.browser.find_element_by_xpath(varsue.meta_secondarygenre_option)
        secGenreOption.click()
        beenDistrib = self.browser.find_element_by_xpath(varsue.meta_beenDistrib)
        beenDistrib.click()
        beenDistribOption = self.browser.find_element_by_xpath(varsue.meta_beenDistrib_option)
        beenDistribOption.click()
        datalang = self.browser.find_element_by_xpath(varsue.meta_datalang)
        datalang.click()
        datalangOption = self.browser.find_element_by_xpath(varsue.meta_datalong_option)
        datalangOption.click()
        recordlang = self.browser.find_element_by_xpath(varsue.meta_recordlang)
        recordlang.click()
        recordlangOption = self.browser.find_element_by_xpath(varsue.meta_recordlang_option)
        recordlangOption.click()
        explicit = self.browser.find_element_by_xpath(varsue.meta_explicit)
        explicit.click()
        explicitOption = self.browser.find_element_by_xpath(varsue.meta_explicit_option)
        explicitOption.click()
        labelQ = self.browser.find_element_by_xpath(varsue.meta_labelq)
        labelQ.click()
        labelQOption = self.browser.find_element_by_xpath(varsue.meta_labelq_option)
        labelQOption.click()
        publisherQ = self.browser.find_element_by_xpath(varsue.meta_publisherq)
        publisherQ.click()
        publisherQOption = self.browser.find_element_by_xpath(varsue.meta_publisherq_option)
        publisherQOption.click()
        tracktype = self.browser.find_element_by_xpath(varsue.meta_tracktype)
        tracktype.click()
        tracktypeOption = self.browser.find_element_by_xpath(varsue.meta_tracktype_option)
        tracktypeOption.click()
        time.sleep(15)

        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testDistribBeforeMaster(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        profile = self.browser.find_element_by_xpath(varsue.vault_clickartist)
        profile.click()
        time.sleep(5)
        locker = self.browser.find_element_by_xpath(varsue.locker_clicklocker)
        locker.click()
        time.sleep(5)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")


if __name__ == '__main__':
    unittest.main(verbosity=2)