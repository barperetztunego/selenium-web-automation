import unittest
import time
import random
import varsue
import string
from seleniumwire import webdriver


class FrontPageTests(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Chrome(
            seleniumwire_options={'verify_ssl': False}
        )
        self.browser.maximize_window()

    def testPageTitle(self):
        self.browser.get(varsue.devUrl)
        self.assertIn(':: TuneGO ::', self.browser.title)

    def getVisibleElementWithName(self, name):
        fields = self.browser.find_elements_by_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def getVisibleElementWithClassName(self, name):
        fields = self.browser.find_elements_by_class_name(name)
        for field in fields:
            if field.is_displayed():
                return field
        return None

    def testGetAllIDs(self):
        self.browser.get(varsue.devUrl)
        time.sleep(10)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(5)
        ids = self.browser.find_elements_by_xpath('//*[@id]')
        for ii in ids:
            print(ii.get_attribute('id'))

    def testNewUser(self):
        randomUsername = "BPeretz+" + ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(4)) + "@tunego.com"
        self.browser.get(varsue.devUrl)
        getStarted = self.browser.find_element_by_xpath(varsue.regis_getstarted)
        getStarted.click()
        time.sleep(3)
        newUsername = self.browser.find_element_by_xpath(varsue.regis_username)
        newUsername.send_keys(randomUsername)
        time.sleep(5)
        newPassword = self.browser.find_element_by_xpath(varsue.regis_password)
        newPassword.send_keys('password')
        confirmPassword = self.browser.find_element_by_xpath(varsue.regis_password_confirm)
        confirmPassword.send_keys('password')
        time.sleep(3)
        acceptTerms = self.browser.find_element_by_xpath(varsue.regis_acceptterms)
        acceptTerms.click()
        submitNewAccount = self.browser.find_element_by_xpath(varsue.regis_submit)
        submitNewAccount.click()
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")

    def testLogin(self):
        self.browser.get(varsue.devUrl)
        username = self.browser.find_element_by_xpath(varsue.login_username)
        username.send_keys(varsue.username_value)
        password = self.browser.find_element_by_xpath(varsue.login_password)
        password.send_keys(varsue.password_value)
        submit = self.browser.find_element_by_xpath(varsue.login_submit)
        submit.click()
        self.assertNotIn(varsue.login_err1 or varsue.login_err2, self.browser.page_source)
        time.sleep(10)
        for request in self.browser.requests:
            if request.response:
                self.assertNotEqual(
                    request.response.status_code,
                    500 or 503 or 403 or 400 or 401,
                    ("Failed at: ", request.path, request.response.status_code)
                )

        print("Test completed.")


if __name__ == '__main__':
    unittest.main(verbosity=2)