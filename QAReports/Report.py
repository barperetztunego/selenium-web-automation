#!/usr/bin/env python
from jinja2 import FileSystemLoader, Environment


# Content to be published


# Configure Jinja and ready the template
env = Environment(
    loader=FileSystemLoader(searchpath="templates")
)
template = env.get_template("report.html")


def main():
    title =  "QA Report"
    header = "QA Report"
    notesHeader = "Notes: "
    referenceHeader = "References: "
    criticalHeader = "Critical Issues: "
    summary = raw_input("Enter report summary: ")

    # Notes array
    notes = []

    # Enter a link to the ticket in JIRA
    criticalIssues = []
    criticalDescs = []

    # Reference links array
    references = []

    while True:
        notePrompt = raw_input("Enter a note: ")
        if notePrompt == "":
            break

        notes.append(notePrompt)

    while True:
        issuePrompt = raw_input("Enter a JIRA issue link: ")
        if issuePrompt == "":
            break
        else:
            criticalIssues.append(issuePrompt)

    while True:
        referencePrompt = raw_input("Enter a Reference link: ")
        if referencePrompt == "":
            break
        references.append(referencePrompt)

    references.append("https://tunego.atlassian.net/browse/BU-475?jql=project%20in%20(UEMOBILE%2C%20BU)%20ORDER%20BY%20created%20DESC")
    references.append("https://tunego.atlassian.net/wiki/spaces/QA/pages/701399041/Feature+Tracking")


    """
    Entry point for the script.
    Render a template and write it to file.
    :return:
    """

    with open("outputs/report.html", "w") as f:
        f.write(template.render(
            title=title,
            header=header,
            summary=summary,
            notes=notes,
            notesHeader=notesHeader,
            referenceHeader=referenceHeader,
            references=references,
            criticalIssues=criticalIssues,
            criticalDescs=criticalDescs,
            criticalHeader=criticalHeader
        ))

    f.close()
    print("Completed Report.")


if __name__ == "__main__":
    main()
