static_url = "https://api.uedev.autotunego.com"
auth_url = "https://auth.uedev.autotunego.com"
switchboard_url = static_url


vault_url = static_url
bridge_url = static_url
switchboard_url = static_url
profile_url = static_url
headroom_url = static_url
journey_url = static_url
# static_url = "http://localhost:3000"
# auth_url = "http://localhost:8000"
# switchboard_url = "http://localhost:6000"
#
#
# vault_url = "http://localhost:3000"
# bridge_url = "http://localhost:3001"
# switchboard_url = "http://localhost:6000"
# profile_url = "http://localhost:8000"
# headroom_url = "http://localhost:8065"
# journey_url = "http://localhost:3002"


end_reg = "/users?client_id=tunegomobile"
end_legacy_reg = "/legacy/users?client_id=tunegomobile"
end_login = "/tokens?client_id=tunegomobile"
projects = "/v2/flow/projects"
lockers = "/v2/vault/lockers"
releases = "v2/flow/projects/{}/releases"
proj_lockers_uri = "/v2/cue/projects/{}/lockers"
invite_uri = "/v2/profile/invitations"
master_url = "/v2/vault/masters"
cue_projects = "/v2/cue/projects"

