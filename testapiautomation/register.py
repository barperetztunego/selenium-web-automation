import requests
import json
import os, random, string
import logging
import base64
from config import *


# code from legacy migration

# logging
logfile = "migrations.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()


# generate random password for user
def generate_password():
    l_length, lu_length = 10, 3
    d_length, c_length = 5, 2
    lower_letters = string.ascii_letters[0:25]
    upper_letters = string.ascii_letters[26:]
    digits = string.digits
    chars = '!@#$%&*'
    random.seed = (os.urandom(1024))
    l_string = ''.join(random.choice(lower_letters) for i in range(l_length))
    lu_string = ''.join(random.choice(upper_letters) for i in range(lu_length))
    d_string = ''.join(random.choice(digits) for i in range(d_length))
    c_string = ''.join(random.choice(chars) for i in range(c_length))
    password = l_string+lu_string+d_string+c_string
    # reorder password string
    password = ''.join(random.sample(password, len(password)))
    return password


# registers and logs in user. Returns an access token
def register_login(email):
    register = "/users?client_id=TG_test"  #"/legacy/users?client_id=TG_test"
    password = "Password1!"  # generate_password()
    email = email.strip()
    payload = {"email": email,
               "password": password}
    headers = {"Content-Type": "application/json"}

    r = requests.post(auth_url + register, headers=headers, data=json.dumps(payload))
    sc_register = r.status_code
    register_response = r.text
    logger.info(str(register_response))

    if sc_register >= 300:
        logger.info("FAILED to register email: " + email)
        return "FAILED"
    else:
        logger.info("SUCCESSFULLY registered user_id: " + email)
        print("SUCCESS")
    # # Login and retrieve access token
    # login = "/tokens?client_id=tunegomobile"
    # login_payload = {"grant_type": "password",
    #                  "client_id": "TG_test",
    #                  "username": email,
    #                  "password": password}
    #
    # r = requests.post(auth_url + login, headers=headers, data=json.dumps(login_payload))
    # sc_login = r.status_code
    #
    # if sc_login >= 300:
    #     logger.info("FAILED to login user_id: " + email)
    #     return "FAILED"
    # else:
    #     login_response = r.json()
    #     access_token = login_response['access_token']
    #     logger.info(r.text)
    #     logger.info("SUCCESSFULLY logged in user_id: " + email)
    #     logger.info("Auth Token Login: " + access_token)
    # return access_token


# 401 = invlid grant
def login(email, password):
    static_url = "https://auth.dev.autotunego.com"
    headers = {"Content-Type": "application/json"}
    login = "/tokens?client_id=tunegomobile"
    login_payload = {"grant_type": "password",
                     "client_id": "tunegomobile",
                     "username": email,
                     "password": password}

    r = requests.post(static_url + login, headers=headers, data=json.dumps(login_payload))
    sc_login = r.status_code
    login_response = r.json()

    access_token = login_response['access_token']
    logger.info(r.text)
    if sc_login >= 300:
        logger.info("FAILED to login user_id: " + email)
        failedLog.info("user_id: " + email)
        return "FAILED"
    else:
        logger.info("SUCCESSFULLY logged in user_id: " + email)
        logger.info("Auth Token Login: " + access_token)
    return access_token


# Create Project(artist) with Artist Name as title
def create_project(access_token, user_id, displayName):
    projects = "/v2/flow/projects"
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    description = "My Artist"
    project_payload = {"displayName": displayName, "description": description}

    r = requests.post(static_url + projects, headers=headers, data=json.dumps(project_payload))
    sc_project = r.status_code
    print(sc_project)
    project_response = r.json()
    print(project_response)
    logger.info(r.text)
    logger.info("Auth Token Project: " + access_token)

    if sc_project >= 300:
        logger.info("FAILED to create project for  user_id: " + user_id)
        failedLog.info("user_id: " + str(user_id))
        # break from loop
        project_url = "FAILED"
        if sc_project == 400:
            project_url = "expired"
    else:
        logger.info("SUCCESSFULLY created project for user_id: " + user_id)
        project_url = project_response['refs']['self']

    return project_url


# Add a Locker per song and video
def create_locker(access_token, project_url, song_title, media_id, description):
    media_id = str(media_id)
    lockers = "/v2/vault/lockers"
    # take out spaces and special characters from song name
    # lockerName = ''.join(e for e in song_title if e.isalnum()).lower()
    lockerName = song_title  # re.sub('[!@#$;*^&()]', '', song_title)

    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}

    locker_payload = {"lockerName": lockerName,
                      "displayName": song_title,
                      "description": description,
                      "in_project": project_url}

    r = requests.post(static_url + lockers, headers=headers, data=json.dumps(locker_payload))
    sc_lockers = r.status_code
    locker_response = r.json()
    print(locker_response)
    logger.info("Auth Token Locker: " + access_token)

    if sc_lockers >= 300:
        logger.info("FAILED to create locker for media_id: " + str(media_id))
        locker_id = "FAILED"
        if sc_lockers == 400:
            locker_id = "expired"
    else:
        logger.info("SUCCESSFULLY created locker for media_id : " + str(media_id))
        locker_id = locker_response['lockerId']
    return locker_id


# Post content to the locker
def add_content(access_token, locker_id, file_name):
    content = "/v2/vault/lockers/{}/branches/0/content".format(locker_id)
    headers = {"AUTHORIZATION": access_token}
    file_type = file_name.split(".")[-1]
    display_name = "song." + file_type
    payload = {display_name: open(file_name, 'rb')}
    r = requests.post(static_url + content, headers=headers, files=payload)
    sc_content = r.status_code
    # content_response = r.json()
    logger.info("Auth Token Content: " + access_token)

    if sc_content >= 300:
        logger.info("FAILED to post content to locker : " + locker_id)
        return "FAILED"
    else:
        logger.info("SUCCESSFULLY posted content to locker: " + locker_id)
    logger.info(r.text)
    return "SUCCESS"


# Update profile with avatar (base 64 encoded string), name, location,
# and social media/music store links
# encode avatar as base64 string
def update_profile(access_token, user_id, first_name, avatar_path, people_id,**kwargs):
    profile = "/v2/profile/people/{}".format(str(people_id))
    if avatar_path == 'no_image':
        encoded_string = None
    else:
        with open(avatar_path, "rb") as image_file:
            encoded_string = base64.b64encode(image_file.read())

        encoded_string = "data:image/jpg;base64," + encoded_string.decode('utf-8')
    if kwargs is not None:
        profile_payload = kwargs.items()
    profile_payload['encoded_string'] = encoded_string
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}

    r = requests.patch(static_url + profile, headers=headers, data=json.dumps(profile_payload))
    sc_profile = r.status_code
    # profile_response = r.json()
    logger.info("Auth Token Profile: " + access_token)

    if sc_profile >= 300:
        logger.info("FAILED to update profile: " + str(user_id))
        return "FAILED" + str(sc_profile)
    else:
        logger.info("SUCCESSFULLY Updated profile: " + str(user_id))
        print("SUCCESS")
    return "SUCCESS"


# retrieve people id
def get_people_id(access_token):
    headers = {"AUTHORIZATION": access_token}
    r = requests.get(static_url + "/v2/profile/people/me", headers=headers)
    if r.status_code >= 300:
        return "FAILED"
    json_response = r.json()
    logger.info("Auth Token People ID: " + access_token)
    return json_response["personID"]


# updates social links for people/people_id
def update_socials(access_token, user_id, people_id,**kwargs):
    user_id, people_id = str(user_id), str(people_id)
    social = "/v2/profile/people/{}".format(people_id)
    if kwargs is not None:
        social_payload = kwargs.items()

    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    r = requests.patch(static_url + social, headers=headers, data=json.dumps(social_payload))
    sc_social = r.status_code
    if sc_social >= 300:
        logger.info("FAILED to updated Socials: " + str(user_id))
        return "FAILED"
    else:
        logger.info("Auth Token Socials: " + access_token)
        logger.info("SUCCESSFULLY Updated socials: " + str(user_id))
    return "SUCCESS"
