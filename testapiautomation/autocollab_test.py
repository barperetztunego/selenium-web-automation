from config import *
import auth
import vault
import logging
import json
import time
import logging
import invitations
import switchboard

# logging
logfile = "test-results.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()


#  case 1: create project, invite, add lockers
def test1():
    access_token1 = auth.login("eyoon+invites4@tunego.com")
    project_url = switchboard.create_project(access_token1, auth.word_generator())
    target_user = "eyoon+invites3@tunego.com"
    print("sending invites...")
    invitations.inviteProject(target_user, project_url, access_token1)
    access_token2 = auth.login(target_user)
    print("retrieing invite ids...")
    invite_urls = invitations.view_invites(access_token2)
    for invite_url in invite_urls:
        print("accepting invite " + invite_url)
        invitations.accept_invites(access_token2, invite_url)
    # trying to access invited project
    response = switchboard.get_project(access_token2, project_url=project_url)
    print("Project info: " + project_url)
    print(response)
    print("creating lockers...")
    child_locker = vault.create_locker(access_token1, project_url, locker_name=auth.word_generator())
    child_locker2 = vault.create_locker(access_token1, project_url, locker_name=auth.word_generator())
    print("Waiting 30 seconds... ")
    time.sleep(30)
    locker_list = switchboard.project_lockers(access_token2, project_url=project_url)
    # print lockers in project
    print("this is the locker list " + locker_list)
    #resp = resp.replace("'", '"')
    #resp = json.loads(resp)
    #try:
    #for i in range(resp):
    locker_uri = locker_list[0]["refs"]["self"]
    if locker_uri == child_locker:
        print("Success")
        #break
    print("Couldn't find shared locker")
    #except Exception as e:
    #    print("Couldn't find shared locker")


#  case 2: create project. add lockers, invite


if __name__ == '__main__':
    test1()
    print("Test Case Passed")

