from config import *
import auth
import switchboard
import requests
import logging
import json
import hashlib
import os
import datetime
import sys
import aiohttp
import concurrent


# logging
logfile = "test-results.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()


# tests vault component. creating/getting lockers and mastering them

# create locker
def create_locker(access_token, project_url, locker_name="Cool Song", fatal=True):
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    lockerName, displayName, description = locker_name, locker_name, "my first song"
    payload = {"lockerName": lockerName,
                      "displayName": displayName,
                      "description": description,
                      "in_project": project_url}

    response = requests.request("POST", static_url + lockers, headers=headers, data=json.dumps(payload))
    print(response.json())
    print("Create locker request: ")
    print(static_url + " : "  + lockers + " : " + json.dumps(payload))
    sc = response.status_code
    if sc < 300:
        logger.info("CREATE Lockers succeeded")
        locker_url = response.json()["refs"]["self"]
        print(locker_url)
        return locker_url
    else:
        print("response code: " + str(sc))
        auth.failed_test("CREATE Lockers", sc, response, fatal)


# async create locker
async def create_locker_async(access_token, project_url,session: aiohttp.ClientSession, locker_name="Cool Song", fatal=True):
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    lockerName, displayName, description = locker_name, locker_name, "my first song"
    payload = {"lockerName": lockerName,
                      "displayName": displayName,
                      "description": description,
                      "in_project": project_url}
    try:
        response = await session.request("POST", static_url + lockers, headers=headers, data=json.dumps(payload))
    except concurrent.futures._base.TimeoutError:
        return None
    print("Create locker request: ")
    print(static_url + " : " + lockers + " : " + json.dumps(payload))
    sc = response.status
    if sc < 300:
        logger.info("CREATE Lockers succeeded")
        locker_json = await response.json()
        locker_url= locker_json["refs"]["self"]
        print(locker_url)
        return locker_url
    else:
        print("response code: " + str(sc))


# use cue projects to list locker info
async def get_proj_lockers_cue(access_token, project_id, session: aiohttp.ClientSession, locker_array=[]):
    headers = {"AUTHORIZATION": access_token}
    url = switchboard_url + cue_projects + '/' + project_id + '/lockers'
    response = await session.request("GET",url,headers=headers)
    sc = response.status
    if sc < 300:
        resp_json = await response.json(content_type='text/plain')
        if resp_json is None:
            print('bridge returned 200 but response was null for project id ' + str(project_id))
            return None  # handles for null response by bridge
        for locker_body in resp_json:
            try:
                locker_uri = locker_body['lockerId']
                locker_array.append(locker_uri)
            except KeyError:
                pass
        return resp_json
    # elif sc == 500:
    #     # try using flow/project/project_id/lockers instead
    #     print('got {} from bridge... trying to use flow endpoint instead'.format(str(sc)))
    #     url = switchboard_url + projects + '/' + project_id + '/lockers'
    #     response = await session.request("GET", url, headers=headers)
    #     sc = response.status
    #     print('flow sc:' + str(sc))
    #     if sc < 300:
    #         resp_json = await response.json()
    #         for locker_uri in resp_json:
    #             locker_array.append(locker_uri)
    else:
        print('couldnt get lockers info for project id ' + project_id + ' status code: ' + str(sc))


def get_lockers(access_token, locker_id=None):
    headers = {"AUTHORIZATION": access_token}
    if locker_id is None:
        url = static_url + lockers
    else:
        url = static_url + locker_id
    response = requests.request("GET",url, headers=headers)
    sc = response.status_code
    if sc < 300:
        logger.info("GET lockers successful")
        # can compare results
        if lockers is None:
            return response.json()
        else:
            return response.text
    else:
        auth.failed_test("GET lockers", sc, response)


# layout for a locker. returns layout response body and # of files
def get_content_info(access_token, locker_id):
    headers = {"AUTHORIZATION": access_token}
    url = static_url + "/v2/vault/lockers/" + str(locker_id) + "/branches/0/layout"
    response = requests.request("GET", url, headers=headers)
    sc = response.status_code
    if sc < 300:
        layout_body = response.json()
        # minus 3 for hidden files
        file_count = len(layout_body['paths'])-3
        logger.info("GET Content info successful")
        return layout_body, file_count
    else:
        auth.failed_test("GET locker content info ", sc, response)


# layout for a locker. returns layout response body and # of files
async def get_content_info_async(access_token, locker_id, file_count_array, session: aiohttp.ClientSession ):
    headers = {"AUTHORIZATION": access_token}
    url = static_url + "/v2/vault/lockers/" + str(locker_id) + "/branches/0/layout"
    response = await session.request("GET", url, headers=headers)
    sc = response.status
    if sc < 300:
        layout_body = await response.json()
        # minus 3 for hidden files
        file_c = len(layout_body['paths'])-3
        file_count_array.append(file_c)
        logger.info("GET Content info successful")
        return layout_body
    else:
        print('couldnt get layout for locker id: ' + locker_id)


# file uploads
def create_content(access_token, locker_uri, file='test.wav',down_file='file.wav'):
    headers = {"AUTHORIZATION": access_token}
    if locker_uri is None:
        return None
    url = static_url + locker_uri + "/branches/0/content"
    files = {down_file: open(file, 'rb')}
    response = requests.post(url, files=files, headers=headers)
    sc = response.status_code
    if sc < 300:
        return response.json()["refs"]["self"]
    else:
        auth.failed_test("POST create content ", sc, response)


# async file uplaods
async def create_content_async(access_token, locker_uri, session: aiohttp.ClientSession, file='test.wav',down_file='file.wav'):
    headers = {"AUTHORIZATION": access_token}
    if locker_uri is None:
        return None
    url = static_url + locker_uri + "/branches/0/content"
    files = {down_file: open(file, 'rb')}
    try:
        response = await session.post(url, data=files, headers=headers)
    except concurrent.futures._base.TimeoutError:
        return None
    sc = response.status
    if sc == 201 or sc == 200:
        try:
            json_resp = await response.json()
            locker_uri = json_resp["refs"]["self"]
        except TypeError:
            locker_uri = 'success but null body'
        print(str(sc) + " " + locker_uri)
        return locker_uri
    else:
        print('failed to add content to locker ' + locker_uri + ' ' + str(sc))


def get_content(access_token, contentUri, file='file.wav'):
    headers = {"AUTHORIZATION": access_token}
    if contentUri == None:
        return None
    url = static_url + contentUri + "/data"
    print(url)
    response = requests.request("GET",url,headers=headers)
    open(file,'wb').write(response.content)
    sc = response.status_code
    if sc < 300:
        logger.info("GET Content successful")
    else:
        auth.failed_test("GET", sc, response)


def initialize_master(access_token, locker_url):
    headers = {"AUTHORIZATION": access_token, "Content-Type": "application/json"}
    url = static_url + locker_url + "/master"
    payload = {"title": "first song"}
    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    sc = response.status_code
    if sc < 300:
        logger.info("Initialize Master successful")
        print("Initialize Master successful")
        return response.json()['masterId'], response.json() # return master id
    else:
        auth.failed_test("Initialize Master", sc, response)


def edit_master(access_token,locker_url, content_url, master_json, masterId):
    masterId = str(masterId)
    headers = {"AUTHORIZATION": access_token, "Content-Type": "application/json"}
    url = static_url + locker_url + '/master/' + masterId
    master_json["content"] = content_url
    master_json["recordingYear"] = 2019
    master_json["genre"] = "K-Pop"
    master_json["subGenre"] = "Rock"
    master_json["language"] = "en"
    master_json["explicit"] = "C"
    master_json["origin"] = "Original"
    master_json["licensor"] = "TuneGO"
    response = requests.request("PUT", url, headers=headers, data =json.dumps(master_json))
    sc = response.status_code
    if sc < 300:
        logger.info("Edit Master Successful")
        print("Edit Master Successful")
        return sc
    else:
        auth.failed_test("Edit Master", sc, response)


def finalize_master(access_token, locker_url, masterId):
    url = static_url + locker_url + '/master/' + str(masterId)
    headers = {"AUTHORIZATION": access_token, "Content-Type": "application/json"}
    payload = {"finalize": True}
    response = requests.request("POST", url, headers=headers, data=json.dumps(payload))
    sc = response.status_code
    if sc < 300:
        logger.info("Finalize Master Successful")
        print("Finalize Master Successful")
        return response.json()
    else:
        auth.failed_test("Finalize Master", sc, response)


def get_masters(access_token, masterId):
    headers = {"AUTHORIZATION": access_token}
    url = static_url + master_url + "/" + str(masterId)
    response = requests.request("GET", url, headers=headers)
    sc = response.status_code
    if sc < 300:
        logger.info("Successfully GOT Mastered locker")
        return response.json()
    else:
        auth.failed_test("GET Masters", sc, response)


# upload download and see its the same file
if __name__ == '__main__':
    email = sys.argv[1]
    access_token = auth.login(email)
    locker_id = sys.argv[2]
    i = 0
    while i < 50:
        print("retrieving content for the " + str(i) + " time")
        get_content_info(access_token, locker_id)
        i += 1
    # add_file = 'large.wav'#'largevault.mov'
    # down_file = 'file.wav' #'largevaultd.mov'
    # email = "eyoon+tg@tunego.com"#sys.argv[1]
    # try:
    #     os.remove(down_file)
    # except OSError:
    #     pass
    # f = open(add_file, mode='rb')
    # contents = f.read()
    # originalSHA = hashlib.sha1(contents)
    # print("Original SHA : " + str(originalSHA.digest()))
    # access_token = auth.login(email)
    # ts = datetime.datetime.now()
    # project_name = ts.strftime('%H:%M:%S %m-%d-%Y')
    # locker_name = ts.strftime('%H:%M:%S %m-%d-%Y')
    # project_url = switchboard.create_project(access_token, project_name)
    # # create locker
    # #project_url = "/v2/flow/projects/449301922720514050"
    # locker_uri = create_locker(access_token, project_url, locker_name)
    # content_uri = create_content(access_token, locker_uri, file=add_file, down_file=down_file)
    # result = get_content(access_token, content_uri, file=down_file)
    # f = open(down_file, mode='rb')
    # downloadedContent = f.read()
    # downloadedSHA = hashlib.sha1(downloadedContent)
    # print("Downloaded SHA : " + str(downloadedSHA.digest()))
    # print(result)
    # if originalSHA.digest() == downloadedSHA.digest():
    #     logger.info("Vault Seems to Be Working")
    #     print("Vault seems to be working")
    # else:
    #     logger.info("Downloaded MD5 hash for content does not match original content.")
    #     print("doesn't match")

