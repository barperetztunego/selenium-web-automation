import requests
import urllib3
import time
import hashlib
import json
import random
import string
import re
import logging
import hashlib
from guerrillamail import GuerrillaMailSession
import stripe



logfile = "test_times.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()


# utils.py
def get_first_master(response):
    return {"first_master_uri": response.json()[0]}

def generate_user(response, **kwargs):
    try:
        user_num = kwargs['user_num']
    except KeyError:
        user_num = "1"
    mail_session = GuerrillaMailSession(email_address="tunegotester_" + kwargs['user_id'])
    print("Test email: " + mail_session.get_session_state()['email_address'])
    if user_num == "2":
        return {"test_email_account2": mail_session.get_session_state()['email_address']}
    else:
        return {"test_email_account": mail_session.get_session_state()['email_address']}

def validate_email(response, **kwargs):
    mail_session = GuerrillaMailSession(email_address="tunegotester_" + kwargs['user_id'])
    mail_id = -1
    for i in range(0,35):
        print("Attempting get validation email, attempt: " + str(i))
        time.sleep(1)
        for mail in mail_session.get_email_list():
            if mail.subject == "Welcome to TuneGO":
                mail_id = mail.guid
        if mail_id != -1:
            break
    if mail_id == -1:
        raise NameError("Failed to get tunego validation email.")
    email = mail_session.get_email(mail_id)
    link_lookups = re.findall(r"\"https://.*?auth.*?tunego.*?\"", email.body)
    validation_link = link_lookups[0].replace('"', '')
    print("validation link: " + validation_link)
    validation_response = requests.get(validation_link, timeout=25)
    if validation_response.status_code != 200:
        raise NameError("Failed to call validation link")

    return {"validation_link": validation_link}


def test_wildcard(response, **kwargs):
    return {"test_wildcard": str({response.json()['refs']['self']: ["GET","POST","PUT","PATCH"]})}

def test_time_stamp(response, **kwargs):
    unixtime = time.time()
    return {"test_time_stamp": str(unixtime)}

def generate_test_run_id(response, **kwargs):
    unixtime = time.time()
    testrunid = kwargs['stackname'] + "-" + str(unixtime)
    print("TEST RUN TRANSACTION/RUN ID: " + testrunid)
    return {"test_run_id": testrunid}

def random_string(response,**kwargs):
    rand_str = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(7)])
    rand_str2 = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(7)])
    return {"random_string": rand_str,"random_string2": rand_str2}

def random_email():
    random_string = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(7)])
    return {"random_email": "eyoon+{}@tunego.com".format(random_string)}

def test_time_stamp(response, **kwargs):
    unixtime = time.time()
    return {"test_time_stamp": str(unixtime)}

def post_locker_content(response, **kwargs):
    url = kwargs['url']
    filename = kwargs['filename']
    headers = {"AUTHORIZATION": kwargs['token'], "TUNEGO-TRANSACTION-ID": kwargs['transaction-id']}
    # option to change the file name as it appears on the platform
    # upname = 'file-' + ''.join([random.choice(string.ascii_letters) for n in range(7)]).lower() + '.wav'
    files = {'file.wav': open(filename, 'rb')}

    try:
        print("posting content to url: " + url)
        create_content_response = requests.post(url, files=files, headers=headers, timeout=30)
        print("Create content response: ")
        print(create_content_response.content)
        print("With status code:")
        print(create_content_response.status_code)
    except urllib3.exceptions.HeaderParsingError:
        print("WARNING: known python exception thrown on upload of content to locker.")

    return {"locker_wav_content_uri": create_content_response.json()['refs']['self']}


def send_invite(response, **kwargs):
    project_id = kwargs['project_id']
    url = kwargs['url']  # "https://postman-echo.com/post"
    access_token = kwargs['token']
    recipient_email = kwargs['user2_email']
    headers = {"Content-Type": "application/json", "AUTHORIZATION": access_token}
    data = {"senderMessage": "my invite", "recipientToken": "{}".format(recipient_email),
            "invitationScope": "/v2/flow/projects/{}".format(project_id), "invitationTemplate": "/v2/profile/roles/templates/2",
            "invitationAccessMap": {"Fixed": {}, "Wildcard": {"/v2/flow/projects/{}".format(project_id): [
                "GET", "POST", "PUT", "PATCH"]}}}
    response = requests.request("POST", url, headers=headers, data=json.dumps(data), timeout=10)
    sc = response.status_code
    print(response.json())
    if sc == 201:
        return {'invite_id': response.json()['inviteId']}
    else:
        print('failed to send invite. status code ' + str(sc))
        assert False


def sha1_hash(filename):
    sha1 = hashlib.sha1()
    BUF_SIZE = 65536
    with open(filename,'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()

def post_flypost_image(response, **kwargs):
    filename = "user_profile.jpg"  #"up_small.jpg"
    sha1 = sha1_hash(filename)
    people_id = kwargs['person_id']
    headers = {"AUTHORIZATION": kwargs['token']}
    files = {'file': ('profile_image.jpg', open(filename, 'rb'), 'multipart/form-data')}
    url = kwargs['url']  # "https://postman-echo.com/post"
    data = {
        "flyer-json": "{\"ScopeURI\":\"/v2/profile/people/" + people_id + "\"}"
    }
    try:
        resp = requests.post(url,data=data, files=files, headers=headers, timeout=30)
    except urllib3.exceptions.HeaderParsingError:
        print("WARNING: known python exception thrown on upload of content to locker.")
    sc = resp.status_code
    if sc == 201 or sc == 200:
        return {"user_flypost_uri": resp.json()['refs']['self'],"user_flypost_sha1": sha1}
    else:
        print("Request to post flypost image failed with status code: " + str(sc))
        assert False


def post_locker_distributable(response, **kwargs):
    url = kwargs['url']
    filename = kwargs['filename']
    headers = {"AUTHORIZATION": kwargs['token'], "TUNEGO-TRANSACTION-ID": kwargs['transaction-id']}
    files = {'cover.jpg': open(filename, 'rb')}

    try:
        create_content_response = requests.post(url, files=files, headers=headers, timeout=30)
        print("Locker Content Uri: " + create_content_response.json()['refs']['self'])
    except urllib3.exceptions.HeaderParsingError:
        print("WARNING: known python exception thrown on upload of content to locker.")

    return {"locker_cover_content_uri": create_content_response.json()['refs']['self']}


def get_sha_for_response(response, **kwargs):
    open(kwargs['filename'] + ".jpg", 'wb').write(response.content)
    downloadedSHA = hashlib.sha1(response.content)
    return {"last_sha_value": downloadedSHA.digest()}


def check_sha_against_original(response, **kwargs):
    f = open(kwargs['original_filename'], mode='rb')
    originalcontents = f.read()
    originalSHA = hashlib.sha1(originalcontents)
    downloadedSHA = hashlib.sha1(response.content)
    if originalSHA.digest() != downloadedSHA.digest():
        raise ValueError('original file sha {} does not match downloaded file sha {}.'.format(
            originalSHA.hexdigest(), downloadedSHA.hexdigest()))

    open(kwargs['filename'] + ".jpg", 'wb').write(response.content)
    return {"last_sha_value": downloadedSHA.digest()}


def list_access_maps(response, **kwargs):
    uris = json.loads(response.content)
    headers = {"AUTHORIZATION": kwargs['token']}
    for uri in uris:
        print(uri)
        access_map_response = requests.get(kwargs['base_url'] + uri + "/access", headers=headers, timeout=10)
        print(access_map_response.content)

def output_response(response, **kwargs):
    print("Content:")
    print(response.content)

def check_layout(response, **kwargs):
    json_resp = response.json()
    content_uri = kwargs['content_uri']
    filename = kwargs['filename']
    content_info = json_resp['paths'][filename]
    if content_info['contentId'] == content_uri:
        return True
    else:
        print("could not match content uri to anything in the locker layout")
        assert False  # couldn't find content uri


def check_layout(response, **kwargs):
    json_resp = response.json()
    content_uri = kwargs['content_uri']
    filename = kwargs['filename']
    content_info = json_resp['paths'][filename]
    if content_info['contentId'] == content_uri:
        return True
    else:
        print("could not match content uri to anything in the locker layout")
        assert False  # couldn't find content uri


# creates stripe order
def stripe_token(response, **kwargs):
    stripe.api_version = '2019-05-16'
    print(response.json())
    stripe.api_key = response.json()['publishable']
    token_json = stripe.Token.create(
     card={
       'number': '4242424242424242',
       'exp_month': 12,
       'exp_year': 2024,
       'cvc': '123',
     },
    )
    token = token_json['id']
    return {"stripe_token": token}


def check_cue(response, **kwargs):
    res_type = kwargs['res_type']
    try:
        response = response.json()
        if len(response) == 0:
            print("returned empty list")
            assert False
        else:
            for resource in response:
                resource_uri = resource['refs']['self']
    except TypeError:
        print("Malformed json response.. No resources returned")
        assert False
    except KeyError:
        print("There was an issue returning some of your " + res_type)
        assert False
    if res_type == 'projects':
        return {'project_id': response[0]['projectId']}
    elif res_type == 'lockers':
        return {'locker_id': response[0]['lockerId']}


