from tavern.core import run
import os

success = run("tavern_tests/switchboard/test_createrelease.tavern.yml","tavern_tests/uat.yaml")


def gather_test_results(result):
    if result:
        test_results = ['Tests succeeded']
    else:
        test_results = ['Tests FAILED']
    log_file = "test_times.log"
    with open(log_file) as fp:
        line = fp.readline()
        while line:
            test_name = line.find("Running test")
            test_time = line.find("Test took")
            if test_name != -1:
                test_results.append(line.strip())
            elif test_time != -1:
                test_results.append(line.strip())
            line = fp.readline()
    fp.close()
    for l in test_results:
        print(l)
    return test_results


# returns a dict of test and the number of seconds it took
def time_breakdowns(test_results):
    i = 1
    second_dict = {}
    while i < len(test_results):
        test_name = test_results[i].split(":")[-1].strip()
        test_time_str = test_results[i+1].split('took')[-1].split('seconds')[0].strip()
        test_time_sec = float(test_time_str)
        second_dict[test_name] = test_time_str
        i += 2
    return second_dict


if not success:
    print("Error running tests. Gathering test_results")
    test_results = gather_test_results(False)
    print(time_breakdowns(test_results))
else:
    # gather info from log files
    print("Test Succeeded. Getting test results...")
    test_results = gather_test_results(True)
    print(time_breakdowns(test_results))


