### Run Your Tavern Test

1. start virtual env

```python3 -m venv env```
2. activate virtual env 

```source env/bin/activate```

3. pip install requirements

```angular2
pip install -r requirements.txt
```

3. Run test. This test runs the distribution test (login to distribution) in the UAT environment
```angular2
PYTHONPATH=$PYTHONPATH:tavern_tests pytest tavern_tests/switchboard/test_createrelease.tavern.yml --tavern-global-cfg=tavern_tests/uat.yaml -vv --tavern-beta-new-traceback
```

pass in a TEST_ID if necessarry
```angular2
TEST_ID=eric_$(date +%s)  PYTHONPATH=$PYTHONPATH:tavern_tests pytest tavern_tests/switchboard/test_createrelease.tavern.yml --tavern-global-cfg=tavern_tests/uedev.yaml -vv --tavern-beta-new-traceback

```

4. Stop Virtual Environemnt

```angular2
deactivate
```

