#  conftest.py

import pytest
import time
import logging

logfile = "test_times.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()


@pytest.fixture(name="time_request")
def fix_time_request():
    t0 = time.time()
    yield
    t1 = time.time()
    logging.info("Test took {} seconds".format(str(t1-t0)))
