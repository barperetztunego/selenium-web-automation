from config import *
import auth
import switchboard
import vault
import invitations
import random
import time
import requests

def vault_test_create(token):
	projUri = switchboard.create_project(token,"Project1")
	lockerUri = vault.create_locker(token,projUri)
	contentUri = vault.create_content(token,lockerUri)
	return {"project":projUri,"locker":lockerUri,"content":contentUri}


def vault_test_retrieve(token,projectUri,lockerUri,contentUri):
	project = switchboard.get_project(token,projectUri)
	locker = vault.get_lockers(token,lockerUri)
	content = vault.get_content(token,contentUri)
	return

def invitation_test(token,email,project):
	inviteURI = invitations.inviteProject(email,project,token)
	token2 = auth.login(email,"Password123!")
	return

def full_test():
	password = "Password123!"
	num1 = random.randint(1000,999999999999999999999)
	num2 = num1 - 1
	email = "rpadua+%d@tunego.com" % num1
	email2 = "rpadua+%d@tunego.com" % num2
	print(email)
	#Register and save the email
	auth.registration(email,password)
	auth.registration(email2,password)
	tempEmail = "rpadua+2@tunego.com"
	time.sleep(30)
	token = auth.login(email,password)
	token2 = auth.login(email2,password)
	if token != None:
		print(token)
		vaultItems = vault_test_create(token)
		vault_test_retrieve(token,vaultItems["project"],vaultItems["locker"],vaultItems["content"])
	print("completed")
	
def multipartTest():
	files = {"file.wav":open("test.wav","rb")}
	response = requests.post('http://httpbin.org/post', files=files)
	print(response.json())
if __name__ == '__main__':
	full_test()
