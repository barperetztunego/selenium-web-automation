TEST_UUID=$(uuidgen)
echo "*********** TEST USER FOR THIS TEST: tunegotester_$TEST_UUID@guerrillamailblock.com"
TEST_ID=$TEST_UUID pytest tavern_tests/switchboard/test_createrelease.tavern.yml --tavern-global-cfg=tavern_tests/prod.yaml --tb=short --junit-xml=/container/release-tests.xml
xsltproc /pytest-xunit.xsl /container/release-tests.xml > /container/tests.xml
rm -f /container/release-tests.xml

