from config import *
import requests
import logging
import random
import string
import os
import json
import time

# logging
logfile = "test-results.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()

# tests Auth Service
# tests registrations and logins


def registration(email,password = "Password1!",skip_confirm = False):
    payload = {"email": email, "password": password}
    headers = {"Content-Type": "application/json"}
    authOption = end_reg
    if skip_confirm:
        authOption = end_legacy_reg
    response = requests.request("POST", auth_url + authOption, headers=headers, data=json.dumps(payload))
    sc = response.status_code
    # resp_headers = response.headers()
    if sc == 201 or sc == 200:
        id = response.json()['id']
        if sc == 201:
            logger.info("REG Test Passed. Generated id " + id)
        else:
            logger.info("Account already created: " + id)
    else:
        failed_test("REGISTRATION", sc, response)


def login(email, password="Password1!"):
    headers = {"Content-Type": "application/json"}
    payload = {"grant_type": "password",
                     "client_id": "tunegomobile",
                     "username": email,
                     "password": password}
    response = requests.request("POST", auth_url + end_login, headers=headers, data=json.dumps(payload))
    print(auth_url + " : " + end_login + " : " + json.dumps(payload))
    sc = response.status_code
    if sc == 200:
        logger.info("LOGIN Test passed")
        return response.json()['access_token']
    else:
        failed_test("LOGIN", sc, response)


def create_email():
    lower_letters = string.ascii_letters[0:25]
    r_string = ''.join(random.choice(lower_letters) for i in range(5))
    email = "eyoon+{}@tunego.com".format(r_string)
    return email


def failed_test(test_name, sc, response, fatal=True):
    logger.info("{} Test FAILED".format(test_name))
    logger.info("Error with response: " + str(sc))
    logger.info(str(response.text))
    # logger.info(str(response.headers()))
    try:
        transaction_id = response.json()['transaction']
        logger.info("Transaction ID: " + str(transaction_id))
    except:
        # try getting transaction id from header
        try:
            transaction_id = response.headers['Tunego-Transaction-Id']
            logger.info("Trasaction ID " + str(transaction_id))
        except KeyError:
            pass
    else:
        logger.info("Couldn't get Transaction ID")
    if sc == 400:
        return "bad token"
    elif sc == 500:
        return 500
    else:
        if fatal:
            # fatal error
            os._exit(0)  # exits
        else:
            return None


def word_generator(length=6):
    lower_letters = string.ascii_letters[0:25]
    l_string = ''.join(random.choice(lower_letters) for i in range(length))
    return l_string



if __name__ == '__main__':
    email = create_email()
    registration(email)
    time.sleep(30)
    login(email)
    logger.info("Auth Service seems to be working")


