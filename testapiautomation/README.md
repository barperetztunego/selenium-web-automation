# Lightweight CUE functional testing

Lightweight functional test script to test API endpoints.

For now, add the test you want to run to the _Dockerfile_

```angular2
docker-compose up --build
```
Look at log file for results of test


This also includes tavern functional tests as well.
In order to run them:

PYTHONPATH=$PYTHONPATH:tavern_tests pytest --tavern-global-cfg=tavern_tests/common.yaml
