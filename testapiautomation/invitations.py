from config import *
import auth
import vault
import requests
import logging
import json
import switchboard
import datetime
import auth

# logging
logfile = "test-results.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()


#  tests for the invitations
def inviteProject(targetUser, projUri, accessToken):
	headers = {
		"Content-Type": "application/json",
		"AUTHORIZATION": accessToken
	}
	wildcard = {
		projUri: ["GET", "PUT", "POST", "PATCH"]
	}
	fixed = {
		
	}
	accessMap = {
		"Fixed": fixed,
		"Wildcard": wildcard
	}
	payload = {
		"senderMessage": "Invite 1",
		"recipientToken": targetUser,
		"invitationScope": projUri,
		"invitationTemplate": "/v2/profile/roles/templates/2",
		"invitationAccessMap": accessMap
	}
	response = requests.request("POST", static_url + "/v2/profile/invitations", headers=headers, data=json.dumps(payload))
	sc = response.status_code
	if sc < 300:
		logger.info("Invite succeeded")
		inviteUri = response.json()["refs"]["self"]
		return inviteUri
	else:
		auth.failed_test("INVITE project", sc, response)


def view_invites(access_token):
	headers = {
		"AUTHORIZATION": access_token
	}
	resp = requests.request("GET", static_url + "/v2/profile/invitations/in", headers=headers)
	sc = resp.status_code
	if sc < 300:
		logger.info("Viewed invites")
		return resp.json()  # returns list of invites
	else:
		auth.failed_test("View Invites", sc, resp)


def accept_invites(access_token, invite_uri):
	headers = {
		"Content-Type": "application/json",
		"AUTHORIZATION": access_token
	}
	response = requests.request("POST", static_url + invite_uri, headers=headers)
	sc = response.status_code
	if sc < 300:
		logger.info("accepted invite")
		return response.json()
	else:
		auth.failed_test("Accepted Invite", sc, response)


# simple invite test
def test_invites():
	try:
		usera = "eyoon+14@tunego.com"
		userb = "eyoon+13@tunego.com"
		access_token1 = auth.login(usera)
		access_token2 = auth.login(userb)
		ts = datetime.datetime.now()
		artist_name = ts.strftime('%H:%M:%S %m-%d-%Y')
		project_uri = switchboard.create_project(access_token1,artist_name)
		invite_uri = inviteProject(userb, project_uri, access_token1)
		print(invite_uri)
		invites_list = view_invites(access_token2)
		if len(invites_list) == 0:
			assert False
		accept_invites(access_token2,invite_uri)
		switchboard.get_project(access_token2,project_uri)
		assert True
	except Exception as e:
		print(e)
		assert False

