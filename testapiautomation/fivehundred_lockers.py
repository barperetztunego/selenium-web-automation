import auth
import string
import random
import datetime
import time
import switchboard
import vault
import asyncio
import aiohttp



# script that creates a thousand projects and a thousand lockers for one user

# one by one
def fivehundred_lockers(project_url, access_token, j=1):
    lockers_created = 0
    locker_start = datetime.datetime.now()
    for i in range(0, 500):
        try:
            name = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(7)])
            locker_uri = vault.create_locker(access_token, project_url, "Song" + name + ' ' + str(i), fatal=False)
        except Exception as e:
            print("locker failed " + str(i))
            print(e)
        # refresh access token
        if locker_uri == "bad token":
            # refresh auth token
            access_token = auth.login(email, password)
            locker_uri = vault.create_locker(access_token, project_url, "Song " + name + ' ' + str(i), fatal=False)
        # wait a bit
        elif locker_uri == 500:
            print("sleeping for 20 seconds...")
            time.sleep(20)
        elif locker_uri is None:
            print("failed to create locker: ")
        else:
            print("created locker p:" + str(j) + " L: " + str(i))
            rand_filename = ''.join([random.choice(string.ascii_letters) for n in range(7)]).lower() + '.wav'
            files_added = 0
            for i in range(0,500):
                try:
                    rand_filename = ''.join([random.choice(string.ascii_letters) for n in range(7)]).lower() + '.wav'
                    vault.create_content(access_token, locker_uri,file='file.wav',down_file=rand_filename) # add a file
                    print('added a file')
                    files_added += 1
                except:
                    continue # couldnt add file
            print('added {} files'.format(str(files_added)))
            lockers_created += 1
    print("500 lockers took: " + str(datetime.datetime.now() - locker_start))
    print("Created " + str(lockers_created) + " lockers")

# one by one
def fivehundred_both(email, password):
    start_time = datetime.datetime.now()
    # start_time: '2019-02-07 23:34:45.514043'
    access_token = auth.login(email, password)
    for j in range(0, 500):
        try:
            name = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(7)])
            project_url = switchboard.create_project(access_token, 'Band' + name + str(j), fatal=False)
        except Exception as e:
            print(project_url + " " + str(j))
            print(e)
        # if token is expired
        if project_url == "bad token":
            access_token = auth.login(email, password)
            try:
                project_url = switchboard.create_project(access_token, 'Band ' + name + ' ' + str(j), fatal=False)
            except Exception as e:
                print(project_url + " " + str(j))
                print(e)
        elif project_url is None:
            pass
        else:
            print("created project " + str(j))
            #project_url = "/v2/flow/projects/467409377867038722"
            fivehundred_lockers(project_url, access_token, j)
    total_time = datetime.datetime.now() - start_time
    print("total process took: " + total_time)


# concurrently create "500" projects
async def fivehundred_projects(proj_array, session):
    name = '500Lock Band ' + ''.join([random.choice(string.ascii_letters + string.digits) for n in range(7)])
    project_url = await switchboard.create_proj_async(access_token, name, session, fatal=False)
    if project_url is not None:
        proj_array.append(project_url)

# concurrently create "500" lockers
async def fivehundred_lockers(project_uri, locker_array, session):
    name = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(7)])
    locker_uri = await vault.create_locker_async(access_token, project_uri,session, "Song" + name, fatal=False)
    if locker_uri is not None:
        locker_array.append(locker_uri)


# concurrently upload "500" wav files
async def fivehundred_files(locker_uri,file_array, session):
    rand_filename = ''.join([random.choice(string.ascii_letters) for n in range(7)]).lower() + '.wav'
    file_uri = await vault.create_content_async(access_token, locker_uri, session, file='file.wav',down_file=rand_filename)
    if file_uri is not None:
        file_array.append(file_uri)


async def create_500(access_token, run_num=25):
    total_start_time = datetime.datetime.now()
    proj_start_time = datetime.datetime.now()
    # create 500 projects
    proj_array = []
    timeout = aiohttp.ClientTimeout(total=30) # set timeout ot 30 seconds
    async with aiohttp.ClientSession(timeout=timeout) as session:
        #await fivehundred_projects(proj_array, session)
        await asyncio.gather(*(fivehundred_projects(proj_array,session) for i in range(run_num)))
    proj_total_time = datetime.datetime.now()-proj_start_time
    # create 500 lockers for each project 500x500
    lockers_start_time = datetime.datetime.now()
    locker_array = []
    for project_url in proj_array:
        async with aiohttp.ClientSession(timeout=timeout) as session:
            await asyncio.gather(*(fivehundred_lockers(project_url,locker_array,session) for i in range(run_num)))
    lockers_total_time = datetime.datetime.now() - lockers_start_time
    # add 500 files to each locker 500x500x500
    file_start_time = datetime.datetime.now()
    file_array = []
    # set files it will try to async at once because platform throws EOF error otherwise
    files_at_once = 10
    loop_times = run_num // files_at_once
    if loop_times == 0:
        loop_times = 1 # makes sure its at least one
    for locker_uri in locker_array:
        loop_num = 0
        while loop_num < loop_times:
            async with aiohttp.ClientSession(timeout=timeout) as session:
                await asyncio.gather(*(fivehundred_files(locker_uri,file_array, session) for i in range(files_at_once)))
            loop_num += 1
    file_total_time = datetime.datetime.now() - file_start_time
    total_time = datetime.datetime.now() - total_start_time
    projects_created,lockers_created, files_created = len(proj_array), len(locker_array), len(file_array)
    print("total time: " + str(total_time) + ", proj time: " + str(proj_total_time) + ", locker time: "
          + str(lockers_total_time) + ", file time: " + str(file_total_time))
    print("projects created: " + str(projects_created))
    print("lockers created: " + str(lockers_created))
    print("files uploaded: " + str(files_created))
    resp_body = {'project_count': projects_created, 'locker_count': lockers_created,
                 'file_count': files_created,
                 'total_time': total_time,
                 'proj_total_time': proj_total_time.total_seconds(),'lockers_total_time':lockers_total_time.total_seconds(),
                 'file_total_time': file_total_time.total_seconds()}
    return resp_body


# see how long it takes to retrieve all the project, lockers, and layouts
async def retrieve_test(access_token):
    total_start_time = datetime.datetime.now()
    proj_start_time = datetime.datetime.now()
    # use cue/projects to list all artist profiles
    project_resp = switchboard.get_cue_projects(access_token)
    proj_total_time = datetime.datetime.now() - proj_start_time
    proj_ids_list = []
    # grabbing data from project response
    for proj_json in project_resp:
        try:
            project_id = proj_json['projectId']
            proj_ids_list.append(project_id)
        except KeyError:
            pass
    proj_total_count = len(proj_ids_list)
    print('Found {} Projects'.format(str(proj_total_count)))
    # call cue/projects/project_id/lockers async
    timeout = aiohttp.ClientTimeout(total=240) # 4 minute timeout limit
    lockers_start_time = datetime.datetime.now()
    locker_array = []
    async with aiohttp.ClientSession(timeout=timeout) as session:
        await asyncio.gather(*(vault.get_proj_lockers_cue(access_token,project_id, session,locker_array=locker_array) for project_id in proj_ids_list))
    lockers_total_time = datetime.datetime.now() - lockers_start_time
    locker_total_count = len(locker_array)
    print('Found {} Lockers'.format(str(locker_total_count)))
    # call layout for each locker v2/vault/lockers/locker_id/branches/0/layout
    # list all lockers user has access to should be from memory
    file_count_array = []
    file_start_time = datetime.datetime.now()
    async with aiohttp.ClientSession(timeout=timeout) as session:
        await asyncio.gather(*(vault.get_content_info_async(access_token,locker_id,file_count_array,session) for locker_id in locker_array))
    total_file_count = sum(file_count_array)
    print('Found {} Files'.format(str(total_file_count)))
    file_total_time = datetime.datetime.now() - file_start_time
    total_time = datetime.datetime.now() - total_start_time
    print('project total time: ' + str(proj_total_time))
    print('locker total time: ' + str(lockers_total_time))
    print('files total time: ' + str(file_total_time))
    print('total time: ' + str(total_time))
    resp_body = {'project_count': proj_total_count, 'locker_count': locker_total_count,
                 'file_count': total_file_count,
                 'total_time': total_time.total_seconds(),
                 'proj_total_time': proj_total_time.total_seconds(),'lockers_total_time':lockers_total_time.total_seconds(),
                 'file_total_time': file_total_time.total_seconds()}
    return resp_body


if __name__ == '__main__':
    # should register a new account
    email, password = "eyoon+500testg@tunego.com", "Password1!"
    # login and get an access token
    access_token = auth.login(email, password)
    # create 500 of each
    asyncio.run(create_500(access_token=access_token))
    # try to retrieve all objects
    resp_down = asyncio.run(retrieve_test(access_token=access_token))
    # run again from memory
    resp_down2 = asyncio.run(retrieve_test(access_token=access_token))
    # checks if 2nd retrieval (from memory) is faster
    if resp_down['total_time'] > resp_down2['total_time']:
        print('was able to retrieve faster the second time')
    else:
        print('first retrieval was faster or the same')

