import vault
import auth
import switchboard
import random
import datetime
import time

# login
access_token = auth.login("eyoon+autouat@tunego.com")


curr_date = datetime.datetime.now().strftime('%H:%M %B %d')

# create project
project_url = switchboard.create_project(access_token, "aTest Master " + curr_date)
#project_url = "/v2/flow/projects/446443962231357441"


master_ids = []
locker_ids = []

# create 5 lockers and master them
i = 0
while i < 5:
    # create locker
    num2 = random.randint(1, 999999999999)
    locker_url = vault.create_locker(access_token, project_url, "Mast. Test " + str(num2))
    # add content
    content_url = vault.create_content(access_token, locker_url)

    # Initialize the master
    masterId, master_json = vault.initialize_master(access_token, locker_url)
    # edit master
    vault.edit_master(access_token, locker_url, content_url, master_json, masterId)
    # Finalize the master
    vault.finalize_master(access_token, locker_url, masterId)
    i += 1
    print("sleeping for 5 seconds")
    master_ids.append(masterId)
    locker_ids.append(locker_url)
    time.sleep(5) # wait 5 seconds


# retrieve the lockers you just mastered
for masId in master_ids:
    r = vault.get_masters(access_token, masId)
    print(r)

for locId in locker_ids:
    r = vault.get_lockers(access_token, locId)
    print(r)
