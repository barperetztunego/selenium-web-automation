from config import *
import auth
import requests
import logging
import json
import asyncio
import aiohttp
import concurrent


# logging
logfile = "test-results.log"
logging.basicConfig(level=logging.INFO, filename=logfile, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging.getLogger().setLevel(logging.INFO)
logger = logging.getLogger()

# tests workflow/switchboard
# tests projects(artists) and releases


# create project(artist)
def create_project(access_token, project_name, fatal=True):
    description = "My Artist"
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    payload = {"displayName": project_name, "description": description}
    response = requests.request("POST", switchboard_url + projects, headers=headers, data=json.dumps(payload))
    print("Create Project request: ")
    print(switchboard_url + " : " + projects + " : " + json.dumps(payload))
    sc = response.status_code
    if sc == 201:
        logger.info("Created a project")
        #  return project url
        project_url = response.json()['refs']['self']
        print(project_url)
        return project_url
    else:
        auth.failed_test("CREATE PROJECT", sc, response, fatal)


# create project(artist)
async def create_proj_async(access_token, project_name,session: aiohttp.ClientSession, fatal=True):
    description = "My Artist"
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    payload = {"displayName": project_name, "description": description}
    try:
        response = await session.request("POST", switchboard_url + projects, headers=headers, data=json.dumps(payload))
    except concurrent.futures._base.TimeoutError:
        return None
    print("Create Project request: ")
    print(switchboard_url + " : " + projects + " : " + json.dumps(payload))
    sc = response.status
    if sc == 201:
        logger.info("Created a project")
        #  return project url
        json_resp = await response.json()
        project_url = json_resp['refs']['self']
        print(project_url)
        return project_url
    else:
        #auth.failed_test("CREATE PROJECT", sc, response, fatal)
        pass


def get_cue_projects(access_token):
    headers = {"AUTHORIZATION": access_token}
    url = switchboard_url + cue_projects
    response = requests.request("GET",url, headers=headers)
    sc = response.status_code
    if sc == 200:
        return response.json()
    else:
        return None


# get projects
def get_project(access_token, project_url=None):
    headers = {"AUTHORIZATION": access_token}
    if project_url is None:
        url = switchboard_url + projects
    else:
        url = switchboard_url + project_url
    response = requests.request("GET", url, headers=headers)
    sc = response.status_code
    if sc < 300:
        logger.info("Able to GET Project")
        if project_url is None:
            return response.text
        else:
            return response.json()
    else:
        auth.failed_test("GET PROJECT", sc, response)


# retrieve project lockers
def project_lockers(access_token, project_url):
    # returns list of locker uris
    headers = {"AUTHORIZATION": access_token}
    project_id = project_url.split("/")[-1]
    url = static_url + proj_lockers_uri.format(project_id)
    resp = requests.request("GET", url, headers=headers)
    sc = resp.status_code
    if sc < 300:
        logger.info("Retrieved Project Lockers successfully")
        resp_json = resp.json()
        return resp_json
    else:
        auth.failed_test("Retrieved Project Lockers", sc, resp)


# edit projects. change display name
def update_project(access_token, displayName):
    payload = {"displayName": displayName}
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    response = requests.request("PATCH", switchboard_url + projects,
                                headers=headers, data=json.dumps(payload))
    sc = response.status_code
    if sc < 300:
        logger.info("PATCH Project succeeded")
    else:
        auth.failed_test("PATCH PROJECT", sc, response)


# releases. first step of release.
def first_post_release(access_token, project_id, title):
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    payload = {"title": title}
    url = switchboard_url + releases.format(project_id)
    response = requests.request("POST", url, headers=headers,
                                data=json.dump(payload))
    sc = response.status_code
    if sc < 300:
        release_id = response.json()['releaseId']
        return release_id
        logger.info("First POST Release succeeded")
    else:
        auth.failed_test("First POST Release", sc, response)


# add necessary fields to release
def edit_release(access_token, project_id, release_id):
    url = switchboard_url + release_id.format(project_id) + "/" + release_id
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    payload = {}  # need to add required fields to payload here
    response = requests.request("PUT", url, headers=headers, data=json.dumps(payload))
    sc = response.status_code
    if sc < 300:
        logger.info("Edit Release succeeded")
    else:
        auth.failed_test("Edit Release", sc, response)


# finalize release
def finalize_release(access_token, project_id, release_id):
    payload = {"finalize": True}
    headers = {"Content-Type": "application/json",
               "AUTHORIZATION": access_token}
    url = switchboard_url + release_id.format(project_id) + "/" + release_id
    response = requests.request("POST", url, headers=headers, payload=json.dump(payload))
    sc = response.status_code
    if sc < 300:
        logger.info("Finalize Release Succeeded")
    else:
        auth.failed_test("Finalize Release", sc, response)


#if __name__ == '__main__':
def test_switchboard():
    email = "eyoon+access1@tunego.com"
    access_token = auth.login(email)
    # create project
    project_url = create_project(access_token, "DJ")
    get_project(access_token,project_url)
    logger.info("Switchboard Seems to Be Working")
    assert True


